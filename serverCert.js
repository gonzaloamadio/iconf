/**
 * Servidor con conexión a base de datos MySQL
 * @module server.js
 */ 


// Load required modules
var https = require("https");              // http server core module
var fs      = require("fs");
var express = require("express");
var bodyParser = require('body-parser');
var hash = require('./pass').hash;
var cookieParser = require('cookie-parser');
var session = require('express-session');
var Imap = require('imap'),
    inspect = require('util').inspect;
var schedule = require('node-schedule');
var async = require('async');
// web framework external module
var io = require("socket.io");         // web socket external module
var easyrtc = require("easyrtc");           // EasyRTC external module
//var redisClient = require('redis').createClient();
//var RedisStore = require('connect-redis')(express);
var nodemailer = require("nodemailer");

// Setup and configure Express http server. Expect a subfolder called "static" to be the web root.
var app = express();

/* ******************************************************************************************/
/* ******************************************************************************************/
/* ******************************************************************************************/
//                            Funciones de consulta de BD
/* ******************************************************************************************/
/* ******************************************************************************************/
/* ******************************************************************************************/
/**
 * Funciones de conexion y consultas de base de datos.
 * @class Base de datos.
 */

var mysql = require('mysql');
var pool = mysql.createPool({
    host: 'localhost',
    database: 'iconfchat',
    user: 'iconfchat',
    password: 'Ichat123'
});

var dbconn = null;

//////////////////////////////////////////////////////////
//   CONEXION
/////////////////////////////////////////////////////////

pool.getConnection(function(err, connection) {
    if (err) {
        console.log("Unable to get connection from pool");
    }
    else {
        connection.query("USE iconfchat;", [], function(error) {
            if (error) {
                console.log("didn't find db iconfchat, have you created it?");
            }
        });
    }
    dbconn = connection;

    console.log("---------------------------A BORRAR SALAS NO PERSISTENTES");
    //deleteAllRows();
    deleteNotPersistentRooms();
});

//Your connection to the MySQL server may close unexpectedly due to an error or you may close it explicitly. If it closes due to some error then you will need to handle that and reopen it if required. The 'close' event is fired when a connection is closed, so we need to handle that.
pool.on('close', function(err) {
    console.log("--------------Entre a pool.on(close), " + new Date());
    if (err) 
    {
        console.log("Se cerro inesperadamente la BD, la abrimos de nuevo");
        // Oops! Unexpected closing of connection, lets reconnect back.
        pool = mysql.createPool({
            host: 'localhost',
            database: 'iconfchat',
            user: 'iconfchat',
            password: 'Ichat123'
        });

        pool.getConnection(function(err, connection) {
            if (err) {
                console.log("Unable to get connection from pool");
            }
            else {
                connection.query("USE iconfchat;", [], function(error) {
                    if (error) {
                        console.log("didn't find db iconfchat, have you created it?");
                    }
                });
            }
            dbconn = connection;

        });
    } 
    else 
    {
        console.log('Connection closed normally.');
    }
});

pool.on('error', function(err) {
    console.log("--------------Entre a pool.on(error), " + new Date());
    if (err) 
    {
        console.log("Se cerro inesperadamente la BD, la abrimos de nuevo");
        // Oops! Unexpected closing of connection, lets reconnect back.
        pool = mysql.createPool({
            host: 'localhost',
            database: 'iconfchat',
            user: 'iconfchat',
            password: 'Ichat123'
        });

        pool.getConnection(function(err, connection) {
            if (err) {
                console.log("Unable to get connection from pool");
            }
            else {
                connection.query("USE iconfchat;", [], function(error) {
                    if (error) {
                        console.log("didn't find db iconfchat, have you created it?");
                    }
                });
            }
            dbconn = connection;

        });
    } 
    else 
    {
        console.log('Connection closed normally.');
    }
});

function restartPool()
{
    console.log("--------------Entre a restartPool(), " + new Date());
    pool = mysql.createPool({
            host: 'localhost',
            database: 'iconfchat',
            user: 'iconfchat',
            password: 'Ichat123'
        });

    pool.getConnection(function(err, connection) {
        if (err) {
            console.log("Unable to get connection from pool");
        }
        else {
            connection.query("USE iconfchat;", [], function(error) {
                if (error) {
                    console.log("didn't find db iconfchat, have you created it?");
                }
            });
        }
        dbconn = connection;

    });
}

//////////////////////////////////////////////////////////
//   ADD
/////////////////////////////////////////////////////////

/**
* Inserta una sala en la tabla "chatrooms", y sus datos utiles en la tabla "roomData"
* @method addRoom
* @param roomName {string} Nombre de la sala que vamos a insertar en la BD
* @param password {string} Password de la sala
* @param queued   {string} Toma los valores "yes" ó "no", dependiendo de si está esperando para ser creada, o si ya está creada.
* @param owner    {string} Nombre de usuario del dueño de la sala
* @param createD  {string} Fecha con el formato "yyyy/mm/dd hh:mm". Fecha en la cual se debe crear la sala. Vacía si no tiene fecha de creación.
* @param expD     {string} Fecha con el formato "yyyy/mm/dd hh:mm". Fecha en la cual se debe borrar la sala. Vacía si no tiene fecha de expirado.
* @param delOnStartup {int} Campo que toma el valor 0 ó 1, dependiendo de si debe recrearse o borrarse en un reseteo de server.
* @param emails   {string} Cadena con mails separados por comas, para enviar avisos de creación de salas. Ejemplo: "pepe@mail.com,pipi@mail.com"
* @param successCB {function} Si fue insertada correctamente, ejecutamos esta función estilo callback
* @param failureCB {function} Si no fue insertada correctamente, ejecutamos esta función estilo callback
*/
//                string,  string,   string, string, string, string, int,        string   string(yes,no)
function addRoom(roomName, password, queued, owner, createD, expD, delOnStartup, emails, invSended, reminder, successCB, failureCB) {
    hash(password, function(err, salt, hash) {
        if (err) {
            failureCB(err, 0);
        }
        else {
            var query = "replace into chatrooms(roomname,salt,hash,queued) values(?,?,?,?)";
            dbconn.query(query, [roomName, salt, hash, queued], function(err, result) {
                if (err) {
                    restartPool();
                    failureCB(err);
                }
                else {
                    var query2 = "replace into roomData(roomname,owner, createDate, deleteDate,deleteOnStartup, emailsInvitados, invitationSended, reminder) values(?,?,?,?,?,?,?,?)";
                    dbconn.query(query2, [roomName, owner, createD, expD, delOnStartup, emails, invSended, reminder], function(err, result) {
                        if (err) {
                            restartPool();
                            failureCB(err);
                        }
                        else {
                            successCB();
                        }
                    });
                }
            });//dbconn.query
        }
    });
}

/*function deleteAllRows(successCB, errorCB){
    var query = "delete from chatrooms";
    dbconn.query(query,null, function(err,res){
        if(err) {
            errorCB(err);
        }else{
            successCB();
        }
    });
}*/

//////////////////////////////////////////////////////////
//   DELEETE
/////////////////////////////////////////////////////////

/**
* Vacia la tabla chatrooms (y por tener delete on cascade, también vacia la tabla roomData).
* @method deleteAllRows
*/

function deleteAllRows(){
    var query = "delete from chatrooms";
    dbconn.query(query,null, function(err,res){
        if(err) {
            console.log("Error al borrar las salas de la base de datos");
        }else{
            console.log("Se borraron las salas de la base de datos exitosamente");
        }
    });
}

function deleteNotPersistentRooms(){
    var query = 'DELETE FROM chatrooms WHERE roomname IN (SELECT roomname FROM roomData WHERE deleteOnStartup=1)';
    dbconn.query(query,null, function(err,res){
        if(err) {
            console.log("Error al borrar las salas de la base de datos. " + err);
        }else{
            console.log("Se borraron las salas no persistenes de la base de datos exitosamente");
        }
    });
}

/**
* @method deleteEntry
* @param roomName {string}
*/
function deleteEntry(roomName){
    var query = "delete from chatrooms where roomname = ? ";

    pool.getConnection(function(err, connection) {
        console.log("-------Entre en [deleteEntry-getConnection]");
        if(err){ 
            restartPool();
            throw err;
        }

        connection.query(query, [roomName], 
            function(err, result) {
                connection.release();
                if (err) {
                    restartPool();
                    console.log("!!!Error al borrar la sala "+roomName+" de la base de datos, puede que quede inconsistente algo.");
                }
                else {
                    console.log("Se borro la entrada correspondiente a la sala: " + roomName);
                }
            }
        );//query
    });//pool
}

/**
* Setea el parametro deleteOnStartup en 1, para que cuando se reinicia el servidor, se borre esa entrada de la BD. Asi ese nombre se puede volver a utilizar para una sala.
* @method deleteOnStartup
* @param roomName {string}
* @param successCB {function}
* @param failCB {function}
*/
function deleteOnStartup(roomName, successCB, errorCB){
    var query = "update roomData set deleteOnStartup=1 where roomname=?";

    pool.getConnection(function(err, connection) {
        console.log("-------Entre en [deleteOnStartup-getConnection]");
        if(err){ 
            restartPool();
            throw err;
        }

        connection.query(query, [roomName], 
            function(err, result) {
                connection.release();
                if (err) {
                    restartPool();
                    errorCB(err);
                }
                else if (result.length === 0) {
                    successCB();
                }
                else {
                    successCB();
                }
            }
        );//query
    });//pool
}

//////////////////////////////////////////////////////////
//   GET
/////////////////////////////////////////////////////////


/**
* Buscamos en la BD si existe una entrada para el nombre pasado como parametro. Si existe en el callback, devolvemos un objeto con los valores de la fila. <br>
* result[0].salt, result[0].hash, result[0].queued
* @method getPasswordEntry
* @param name {string} Nombre que vamos a buscar si existe en la BD
* @param successCB {function} Si name está definido llamamos a calls successCB({salt:value, hash:value}), sino successCB(null);
* @param errorCB {function} Si se produce algún error, ejecutamos esta función estilo callback
*/
//
//  if the user name is defined, calls successCB({salt:value, hash:value})
//  otherwise returns successCB(null);
//
/*function getPasswordEntry(name, successCB, errorCB) {
    var query = "select salt as salt, hash as hash from chatrooms where roomname = ?";
    dbconn.query(query, [name], function(err, result) {
        if (err) {
            errorCB(err);
        }
        else if (result.length === 0) {
            successCB(null);
        }
        else {
            successCB(result[0]);
        }
    });
}*/

function getPasswordEntry(name, successCB, errorCB) {
    var query = "select chatrooms.salt as salt, chatrooms.hash as hash, chatrooms.queued as queued, roomData.deleteOnStartup as deleteOnStartup from chatrooms, roomData where chatrooms.roomname = ? AND roomData.roomname = ?";

    pool.getConnection(function(err, connection) {
        console.log("-------Entre en [getPasswordEntry-getConnection]");
        if(err){ 
            restartPool();
            throw err;
        }

        connection.query(query, [name,name], 
            function(err, result) {
                connection.release();
                if (err) {
                    restartPool();
                    errorCB(err);
                }
                else if (result.length === 0) {
                    successCB(null);
                }
                else {
                    successCB(result[0]);
                }
            }
        );//query
    });//pool
}

function getEntry(name, successCB, errorCB) {
    var query = "select chatrooms.* , roomData.* from chatrooms, roomData where chatrooms.roomname = ? AND roomData.roomname = ?";

    pool.getConnection(function(err, connection) {
        if(err){ 
            console.log("--------------Entre a getEntry (err), " + Date());
            restartPool();
            throw err;
        }

        connection.query(query, [name,name], 
            function(err, result) {
                connection.release();
                if (err) {
                    restartPool();
                    errorCB(err);
                }
                else if (result.length === 0) {
                    successCB(null);
                }
                else {
                    successCB(result[0]);
                }
            }
        );//query
    });//pool
}


/**
* Ejecuta alguna query estatica, devuelve un array con todas las filas.
* @method executeQuery
* @param query {string} Query estática que vamos a ejecutar
* @param successCB {function(Obj)} Callback que llamamos si se ejecuto con éxito la query. La llamamos con null si no hubo resultados, o array con resutltados. SuccessCB(null), SuccessCB(result)
* @param errorCB {function(err)} Callback que llamamos si no se ejecuto con éxito la query
* @return A SuccessCB, la llamamos con toda las filas resultantes. Ej, result[0].roomname, result[0].queued, result[1].roomname, etc.
*/
function executeQuery(query,successCB,errorCB)
{
     pool.getConnection(function(err, connection) {
        if(err){ 
            console.log("--------------Entre a executeQuery (err), " + Date());
            restartPool();
            throw err;
        }

        connection.query(query, null, 
            function(err, result) {
                connection.release();
                if (err) {
                    restartPool();
                    errorCB(err);
                }
                else if (result.length === 0) {
                    successCB(null);
                }
                else {
                    successCB(result);//result[0].roomname, result[1].roomname, etc.
                }
            }
        );//query
    });//pool   
}

/**
* Chequea la verasidad de los datos que se le proveen. Mas precisamente si los dos campos pasados coinciden con una entrada de la BD.
* @method checkPasswordRoom
* @param name {string} 
* @param password {string} 
* @param successCB {function} Si name y password son una entrada correcta de la BD, llamamos a successCB(name)
* @param errorCB {function} Si name y password NO son una entrada correcta de la BD llamamos a failCB(reason, code)
*/
function checkPasswordRoom(name, password, successCB, failCB) {
    getPasswordEntry(name,
            function(entry) {
                if (!entry) {
                    failCB("No such room", 0);
                }
                else {
                    hash(password, entry.salt, function(err, hash) {
                        if (err) {
                            failCB("hash failure:" + err, 1);
                        }
                        else if (hash !== entry.hash) {
                            failCB("password doesn't match", 2);
                        }
                        else {
                            successCB(name);
                        }
                    });
                }
            },
            function(message, code) {
                failCB("database error:" + message, code);
            }
    );
}

//////////////////////////////////////////////////////////
//   UPDATE
/////////////////////////////////////////////////////////

/**
* Actualiza en la tabla, el campo "campo" con el nuevo valor "valor"
* @method unqueueRoom
* @param roomName {string}
* @param cb {function}
*/
function unqueueRoom(roomName,cb) {
    var query = "update chatrooms set queued='no' where roomname = ?";

    var inserts = [roomName];
    query = mysql.format(query, inserts);
    
    console.log(query);

    pool.getConnection(function(err, connection) {
        if(err){ 
            console.log("--------------Entre a updateEntryChatrooms (err), " + new Date());
            restartPool();
            throw err;
        }

        connection.query(query, null, 
            function(err, result) {
                connection.release();
                if (err) {
                    console.log("[unqueueRoom] -  La sala "+roomName+ " no se puedo desencolar");
                    //errorCB(err);
                    cb(err);
                }
                else {
                    console.log("[unqueueRoom] -  La sala "+roomName+ " se desencolo");
                    //successCB();
                    cb();
                }
            }
        );//query
    });//pool
}

/**
* Marca el campo que nos dice si ya se mando la invitacion.
* @method setAlreadyNotified
* @param roomName {string}
* @param cb {function}
*/
function setAlreadyNotified(roomName,cb) {
    var query = "update roomData set invitationSended='yes' where roomname = ?";

    var inserts = [roomName];
    query = mysql.format(query, inserts);
    
    console.log(query);

    pool.getConnection(function(err, connection) {
        if(err){ 
            console.log("--------------Entre a updateEntryChatrooms (err), " + new Date());
            restartPool();
            throw err;
        }

        connection.query(query, null, 
            function(err, result) {
                connection.release();
                if (err) {
                    console.log("[unqueueRoom] -  La sala "+roomName+ " no se pudo marcar como que fue enviada la invitacion");
                    //errorCB(err);
                    cb(err);
                }
                else {
                    console.log("[unqueueRoom] -  La creación de sala "+roomName+ " fue notificada a los usuarios invitados.");
                    //successCB();
                    cb();
                }
            }
        );//query
    });//pool
}

/* ******************************************************************************************/
/* ******************************************************************************************/
/* ******************************************************************************************/
//             CREACION/BORRADO DE SALAS PROGRAMADAS, PERSISTENTES.
/* ******************************************************************************************/
/* ******************************************************************************************/
/* ******************************************************************************************/

//If we have the number 9, display 09 instead
function addZero(num){
  return (num<10?"0":"")+num;
}

/**
 * Funciones varias
 * @class Funciones de utilidad
 */

/**
* Convierte la javascript Date, fecha y hora actual, al formato que estamos usando. yyyy/mm/dd hh:mm
* @method parseDate
*/
function parseDate(d)
{
    var res="";
    res += d.getFullYear()+"/"+addZero(d.getMonth()+1)+"/"+addZero(d.getDate())+" "+d.getHours()+":"+d.getMinutes(); 
    
    return res;
}

/**
* Funcíon que compara dos fechas. Las fechas son tomadas de la BD y fueron guardadas por el datepicker, y tienen el formato: "yyyy/mm/dd hh:mm"
* El segundo parametro vacío lo tomamos como que es nada. Si comparamos con nada, es menor.
* @method greaterThanDate
* @param d1 {string} String representing a date, with format "yyyy/mm/dd hh:mm"
* @param d2 {string} String representing a date, with format "yyyy/mm/dd hh:mm". If empty, d2 > d1.
*/
function greaterThanDate(d1,d2){

    //Si lo comparamos con nada, consideramos que es menor.
    if (d2=="") {
        return false;
    };

    var partsd1 = d1.split(' '); //partsd1[0] = "yyy/mm/dd", partsd2[1] = "hh:mm"
    var partsd2 = d2.split(' ');

    var dd1 = partsd1[0].split('/'); //dd1[0] = "yyyy", dd1[1] = "mm", dd1[2] = "dd"
    var dd2 = partsd2[0].split('/');

    var hd1 = partsd1[1].split(':'); //hd1[0] = "hh", hd1[1] = "mm"
    var hd2 = partsd2[1].split(':');
    //var d = new Date(year, month, day, hours, minutes, seconds, milliseconds);
    var date1 = new Date(dd1[0],dd1[1]-1,dd1[2],hd1[0],hd1[1]);
    var date2 = new Date(dd2[0],dd2[1]-1,dd2[2],hd2[0],hd2[1]);

    //console.log("Date1="+date1);
    //console.log("Date2="+date2);

    var res = (date1 > date2) ?  true : false;
    return res;
}

/** 
* Nos devuelve un javascript Date, a partir de nuestro formato "yyyy/mm/dd hh:mm"
* @method myDateToJsDate
* @param d1 {string} String con una fecha en formato "yyyy/mm/dd hh:mm"
*/
function myDateToJsDate(d1){

        //Si lo comparamos con nada, consideramos que es menor.
    if (d1=="") {
        return "";
    };
    var partsd1 = d1.split(' '); //partsd1[0] = "yyy/mm/dd", partsd2[1] = "hh:mm"
    var dd1 = partsd1[0].split('/'); //dd1[0] = "yyyy", dd1[1] = "mm", dd1[2] = "dd"
    var hd1 = partsd1[1].split(':'); //hd1[0] = "hh", hd1[1] = "mm"
    return new Date(dd1[0],dd1[1]-1,dd1[2],hd1[0],hd1[1]);
}


////////////////////////delete programmed rooms/////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
function deleteQueuedRoom(){
    function suc(entry)
    {
        if (!entry) {
            console.log("[deleteQueuedRoom] - No hay sala programada para ser borrada.");
        } 
        else
        {   
            var d = parseDate(new Date());
            var datos = []; //nombres de las salas a crear
            //armamos el array con los datos, para poder aplicar la asynch library
            for (var i = 0; i < entry.length; i++) {
                if(greaterThanDate(d,entry[i].deleteDate)) //si la hora actual, es mayor a la que dice que tiene que ser borrada, la ponemos para borrar.
                    datos.push(entry[i].roomname);    
            }

            deleteQueuedRoomAux(datos);
        }//else
    }

    function fail()
    {
        console.log("-------------------------- Error leyendo la BD");
    }

    executeQuery("select * from roomData where deleteDate != ''",suc, fail);
}

// Recibe un [string], y a cada uno le aplica asyncDelete.
function deleteQueuedRoomAux(datos)
{
    async.each(datos, asyncDelete, function (err, results) {
        if(err) console.log("Termino de borrar salas programada con error,"+ new Date()+", Paso lo siguiente: " + err );
        else {
            easyRtcAppObj.getConnectionEasyrtcids(function(err, arr) {

                  for(var i = 0; i < arr.length; i++){
                       easyRtcAppObj.connection(arr[i],function(err, connObj){ //get connObj for every user
                            connObj.emitRoomDataDelta(false, dummy);
                            connObj.generateRoomDataDelta(false, dummy);
                            connObj.generateRoomList(dummy);
                        });
                   }
            });
            console.log("Termino de borrar salas programada con exito." + new Date());
        }
    });
}

function dummy(err,obj) {}

/**
* Para cada roomname en en los datos de createQueuedRoomAux, hacemos el update de deleteOnSatrtup (la ponemos para ser borrada)
* @method 
*/
function asyncDelete(roomName, doneCallback){
    //deleteOnStartup(roomName,doneCallback,doneCallback);
    easyRtcAppObj.deleteRoom(roomName, function(err) {
        if (err) {
            console.log("[onEasyrtcMsg deleteRoom - failDelete] No se pudo borrar de manera programada la sala : " + roomName+" . Paso lo siguiente: " + err);
            doneCallback(null);
        } else{
            console.log("[onEasyrtcMsg deleteRoom - succesDelete] Se borró de manera programada la sala : " + roomName);
            deleteEntry(roomName); //Si no se llega a borrar, dsps cuando creen una sala, como se inserta con replace, no hay conflicto.
            doneCallback(null);
        };
    });
}
////////////////////////create programmed rooms/////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
/**
* Traemos desde la BD todas las salas que esten encoladas (queued="yes"), y tengan seteada alguna fecha de creación. <br>
* A partir de todas esas entradas, nos fijamos cuales son las
* @method createQueuedRoom
*/
function createQueuedRoom()
{
    console.log("-------Start running createQueuedRoom");

    function suc(entry)
    {
        if (!entry) {
            console.log("[createQueuedRoom] - No se crean reuniones, ya que no hay salas programadas esperando.");
        } 
        else
        {   
            var d = parseDate(new Date());
            var datos = []; //nombres de las salas a crear
            //armamos el array con los datos, para poder aplicar la asynch library
            for (var i = 0; i < entry.length; i++) {
                if(greaterThanDate(d,entry[i].createDate))
                    datos.push(entry[i].roomname);    
            }

            createQueuedRoomAux(datos);
        }//else
    }

    function fail()
    {
        console.log("-------------------------- Error leyendo la BD");
    }

    executeQuery("select * from roomData,chatrooms where chatrooms.roomname = roomData.roomname and roomData.createDate != '' and chatrooms.queued='yes'",suc, fail);
}

// Recibe un [string], y a cada uno le aplica asyncCreate.
function createQueuedRoomAux(datos)
{
    async.each(datos, asyncCreate, function (err, results) {
        if(err) console.log("Termino crear salas programadas, "+ new Date()+", Error: " + err);
        else {
            easyRtcAppObj.getConnectionEasyrtcids(function(err, arr) {

                  for(var i = 0; i < arr.length; i++){
                       easyRtcAppObj.connection(arr[i],function(err, connObj){ //get connObj for every user
                            connObj.emitRoomDataDelta(false, dummy);
                            connObj.generateRoomDataDelta(false, dummy);
                            connObj.generateRoomList(dummy);
                        });
                   }
            });
            console.log("Termino crear salas programadas Ok, " + new Date());
        }
    });
}
//Funcion pasada en el async.each, toma un string (uno por cada uno de datos).
function asyncCreate(roomName, doneCallback)
{
    async.series([
        function(callback){
            console.log("-------Chequeando si la sala " + roomName + " esta creada.");
            //Nos fijamos si la sala esta creada, y le pasamos el callback para que siga o corte.
            alreadyCreatedAsync(roomName, callback);
        },
        function(callback){
            console.log("-------Desencolando la la sala " + roomName);
                unqueueRoom(roomName, callback);
        },
        function(callback){
            function suc(roomName){
                createPubRoom(roomName);                        
                callback(null);
            }
            //si la sala tenia password.
            function fail(errText, errCode){
                createPrivRoom(roomName);
                callback(null);
            }
            //Nos fijamos si tiene o no password, para saber cual de los dos app.get crear
            checkPasswordRoom(roomName, "", suc, fail);
            
        }
    ],
    // optional callback
    function(err, results){
        doneCallback(null);//Siempre null aca, para q no se corte la ejecucion para el resto de los datos.
    });
}

/**
* Función que nos dice si una sala ya esta creada.
* @method alreadyCreatedAsync
* @param roomName {string} Nombre de la sala que vamos a chequear
* @param cb {function} Callback llamado según si existe o nó. Si existe la sala, llamamos a cb(errString), si no existe a cb(null);
*/
//Si ya esta entre los nombres de salas, llamamos a err, sino existe la sala llamamos a suc.
function alreadyCreatedAsync(roomName, cb)
{
    function foo(err, arr)
    {
        var flag = false;

        for (var j = 0; j < arr.length; j++) {
            if (arr[j] == roomName) { flag = true;};
        }

        if (flag) {
            cb("Ya fue creada la sala");
        } else{
            cb(null);
        };
        //cb(flag); //cb(true) si fue creada.
    }

    easyRtcAppObj.getRoomNames(foo);
    //suc();
}

/**
* Crea una nueva sala pública (sin password), y la pública para poder entrar a ella por URL.
* @method createPubRoom
* @param nombre {string} Nombre de la sala a crear
*/
function createPubRoom(nombre){
    easyRtcAppObj.createRoom(nombre, null, function(err, roomObj) {
            if (err) throw err;
            console.log("Room " + nombre + " has been created.");
        });

    app.get('/'+nombre, function(req, res, next) {
            res.cookie("roomName", nombre);
            res.sendfile('/indexSalaPub.html',{root:"./static"});
    });
}

/**
* Crea una nueva sala privada (con password), y la pública para poder entrar a ella por URL.
* @method createPrivRoom
* @param nombre {string} Nombre de la sala a crear
*/
function createPrivRoom(nombre){
    easyRtcAppObj.createRoom(nombre, null, function(err, roomObj) {
            if (err) throw err;
            console.log("Room " + nombre + " has been created.");
        });

    app.get('/'+nombre, function(req, res, next) {
            res.cookie("roomName", nombre);
            res.sendfile('/indexSala.html',{root:"./static"});
    });
}


////////////////////////Create rooms on startup/////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
/**
* Crea las salas que no estén marcadas como para ser borradas. Para cuando se resetea el server.
* @method createPersistentRooms
*/
function createPersistentRooms()
{
    function suc(entry){
        if (!entry) {
            console.log("---------------------------no hay nada en la BD para crear");
        } 
        else
        {   //si hay entrada, creamos todas las salas que haya que crear.
            var funcs = [];
            function createfunc(nombre) {
                return function() {
                    //si es una sala sin password.
                    function suc(roomName){
                        createPubRoom(nombre);                        
                    }
                    //si la sala tenia password.
                    function fail(errText, errCode){
                        createPrivRoom(nombre);
                    }
                    //Nos fijamos si tiene o no password, para saber cual de los dos app.get crear
                    checkPasswordRoom(nombre, "", suc, fail);
                };
            }//createFunc
          
            //Creamos una funcion para cada entrada, ya que adentro tenemos operaciones asincronas. Sino daba cualquier cosa.
            for (var i = 0; i < entry.length; i++) {
                funcs[i] = createfunc(entry[i].roomname);
            }

            for (var j = 0; j < entry.length; j++) {
                funcs[j]();
            } 
        };
    }

    function fail(){
        console.log("-------------------------- Error leyendo la BD");
    }
    //Buscamos las salas que tengan deleteOnStartup=0 y queued="no", o sea, no estén para ser borradas y no estén planificadas para crear en futuro.
    executeQuery("SELECT chatrooms.roomname FROM roomData,chatrooms WHERE chatrooms.roomname = roomData.roomname AND chatrooms.queued='no' AND roomData.deleteOnStartup=0", suc, fail);
}

////////////////////////Send mail invitations///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
//tomamos la fecha guardada (fg). esa fecha menos un dia.
//Nos fiamos que la fecha actual sea mayor a esa. Para que se mande una sola vez.
//Lo corremos a las y cuarto de todas las horas.
function sendInvitationMails()
{
    console.log("-------Start running sendInvitationMails");

    function suc(entry){
        if (!entry) {
            console.log("[sendInvitationMails] - No se envian invitaciones, ya mandaron todas, o no hay lista de emails para mandar");
        } 
        else{   
            var  now = new Date(); // ahora
            var datos = [];
            var datosTemp=[];
            for (var i = 0; i < entry.length; i++) {
                var fechaAviso = myDateToJsDate(entry[i].reminder); 
                //fechaAviso.setDate(fechaAviso.getDate()-1); //fecha de creacion menos un dia

                if(now > fechaAviso /*&& now < fechaCre2*/){
                    datosTemp = [entry[i].roomname, entry[i].owner,entry[i].createDate, entry[i].emailsInvitados];
                    console.log("datos Temp: "+datosTemp);
                    datos.push(datosTemp);    
                }
            }
            sendMails(datos);
        }//else
    }

    function fail(){
        console.log("-------------------------- Error leyendo la BD para mandar invitaciones a salas por e-mail");
    }
    //Obtenemos las que todavía no estén creadas, que no se haya mandado la invitacion, que haya emails, y que haya fecha seteada para mandar.
    executeQuery("select * from roomData,chatrooms where chatrooms.roomname = roomData.roomname and roomData.createDate != '' and chatrooms.queued='yes' and roomData.invitationSended='no' and roomData.reminder != '' and roomData.emailsInvitados != ''",suc, fail);
}

// Recibe un [[string]], y a cada uno le aplica sendMailsAux.
function sendMails(datos)
{
    async.each(datos, sendMailsAux, function (err, results) {
        if(err) console.log("Termino de mandar mails ,Error: " + err);
        else console.log("Termino de mandar mails Ok");
    });
}

function sendMailsAux(eachDatos, doneCallback){
    async.series([
        function(callback){
            setAlreadyNotified(eachDatos[0],callback);
        },
        function(callback){

            var mailOptions = {
            from: eachDatos[1], // sender address
            to: eachDatos[3], // list of receivers
            subject: "Invitacion a la reunión "+ eachDatos[0], // Subject line
            text: "Estimado, Le envio  la direccion para que pueda entrar en la reunión a la que le estoy invitando. La reunión comienza: "+eachDatos[2]+ " - https://iconf.t-servicios.com.ar/"+eachDatos[0]+"  , Atte : "+eachDatos[1], // plaintext body
                //html: "<a href='https://iconf.t-servicios.com.ar/'"+eachDatos[0]+"> Link a la reunión </a>" // html body
            }

            smtpTransport.sendMail(mailOptions, function(error, response){
                if(error){
                    console.log(error);
                }else{
                    console.log("Message sent: " + response.message);
                }
            });
            callback(null);
        }
    ],
    // optional callback
    function(err, results){
        doneCallback(null);
    });
}

// Recibe un [[string]], y a cada uno le aplica sendMailsAux2.
//Envia mails, apenas se programa la creacion de sala.
function sendMailsOnQueued(datos)
{
    async.each(datos, sendMailsAux2, function (err, results) {
        if(err) console.log("Termino de mandar mails ,Error: " + err);
        else console.log("Termino de mandar mails Ok");
    });
}

function sendMailsAux2(eachDatos, doneCallback){
    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: eachDatos[1], // sender address
        to: eachDatos[3], // list of receivers
        subject: "Invitacion a la reunión "+ eachDatos[0], // Subject line
        text: "Estimado, Le envio  la direccion para que pueda entrar en la reunión a la que le estoy invitando. La reunión comienza: "+eachDatos[2]+ " - https://iconf.t-servicios.com.ar/"+eachDatos[0]+"  , Atte : "+eachDatos[1], // plaintext body
        //html: "<a href='https://iconf.t-servicios.com.ar/'"+eachDatos[0]+"> Link a la reunión </a>" // html body
    }

    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log(error);
        }else{
            console.log("Message sent: " + response.message);
        }
    });

    //continuamos consumiendo el siguiente dato en datos (el parametro de sendMails)
    doneCallback(null);
}
// ==================end of database specific logic======================= //

/* ******************************************************************************************/
/* ******************************************************************************************/
/* ******************************************************************************************/
//                                 ROUTING
// this file is a mashup between the auth example in expression and the server example in easyrtc.
/* ******************************************************************************************/
/* ******************************************************************************************/
/* ******************************************************************************************/

/**
 * Función para loguearse mediante IMAP
 * @class Conexion a la aplicacion
 */
//Nos permite leer cosas submited by post forms
app.use(bodyParser());
app.use(cookieParser('shhhh, very secret'));
app.use(session());

/*app.use(cookieParser());
app.use(express.session({
  store: new RedisStore({
    host: 'localhost',
    port: 6379,
    db: 0,
    cookie: { maxAge: (24*3600*1000*30)}, // 30 Days in ms
    client : redisClient
    //pass: 'RedisPASS'
  }),
  secret: 'estoesungransecretoestoesungransecreto'
}));
*/

/**
* Función de logueo mediante IMAP, se fija que exista un usuario con el mail provisto en login. Si existe, puede entrar, sino no.
* @method checkPassword
* @param name {string} Usuario en el mail
* @param server {string} Servidor de mail
* @param password {string} Password
* @param successCB {function} Si el logueo contra Imap es satisfactorio, llamamos a successCB(name)
* @param errorCB {function} Si el logueo contra Imap es satisfactorio, llamamos a failCB(reason, code)
*/

function checkPassword(userName, server, password, successCB, failCB) {


    userName = userName + "@" + server;
    var hostName;
    var portNumber;
    var tlsEnable;
    
    //Harcodeamos un usuario para poder entrar y probar.
    if(userName=="itecnis@ola.com.ar"){
    successCB("Itecnis");
    return;
    }
    
    console.log("Autenticando a "+userName);

    switch(server){
        case "ola.com.ar":
            hostName = "10.60.1.131";
            portNumber = 993;
            tlsEnable = true;
            break;
        case "t-servicios.com.ar":
            hostName = "10.60.1.142";
            portNumber = 993;
            tlsEnable = true;
            break;
        case "transatlantica.com.ar":
            hostName = "10.60.1.2";
            portNumber = 993;
            tlsEnable = true;
            break;
        case "itecnis.com":
            hostName = "imap.gmail.com";
            portNumber = 993;
            tlsEnable = true;
            break;
        case "grupogamma.com":
            hostName = "correo.grupogamma.com";
            portNumber = 993;
            tlsEnable = true;
            break;
        default:
            failCB("Servidor incorrecto",0);
            //next(new easyrtc.util.ConnectionError("Dominio incorrecto. Ha fallado la autenticacion. Intente nuevamente."));
    }

    var imap = new Imap({
      user: userName,
      password: password,
      host: hostName,
      port: portNumber,
      tls: tlsEnable,
      tlsOptions: { rejectUnauthorized: false }
    });

    function openInbox(cb) {
      imap.openBox('INBOX', true, cb);
    }

    imap.once('ready', function() {
        console.log("Autenticacion exitosa de "+userName);
        successCB(userName);
        imap.end();
    });

    imap.once('error', function(err) {
        console.log("Autenticacion erronea de "+userName);
        failCB("Password incorrecta. Intente nuevamente.",1);
    });

    imap.once('end', function() {
        console.log('Cerrando comunicacion IMAP.');
    });

    imap.connect();
    
}


////////////// configuring the http server ///////////////////////

app.set("views", __dirname);


//~ app.get('/createaccount', function(req, res, next) {
    //~ res.sendfile('create_account.html', {root: './login'});
//~ });

app.get('/', function(req, res, next) {
    res.redirect('/index.html');
});

app.get('/logout', function(req, res, next) {
    //~ var toDelete = "/"+req.session.user;
    // destroy the user's session to log them out
    // will be re-created next request
    req.session.destroy(function() {
        res.sendfile('login.html', {root: './static'});
    });
    //~ for(var i=0;i < app.routes.get.length; i++)
        //~ if(app.routes.get[i].path==toDelete)
            //~ app.routes.get.splice(i,1);
});

app.get('/logout2', function(req, res, next) {
    req.session.destroy(function() {
        res.redirect("/"+req.cookies.roomName);
    });
});



app.get('/login', function(req, res, next) {
    res.sendfile('login.html', {root: './static'});
});

//
// this function is used to check if you are authenticated
// when trying to enter a demo page.
//
function restrict(req, res, next) {
    if (req.session.user) { //req.cookies.name
    //    if (req.cookies.testing == "itecnis"){
        next();
    } else {
        req.session.error = 'Access denied!';
        res.cookie("redirect_to",req.url); //guardamos una cookie con la página a la que intento entrar, asi cuando se autentica, lo redirigimos ahí.
        res.redirect('/login'); //como no estaba autenticado, lo mandamos a loguearse.
    }
}


// Publico CSS
app.get(/^\/css\/.+/,function(req,res){
    res.sendfile(req.url,{root:"static"});
});

// Publico JS
app.get(/^\/js\/.+/,function(req,res){
    res.sendfile(req.url,{root:"static"});
});

app.get(/^\/plugin\/.+/,function(req,res){
    res.sendfile(req.url,{root:"static"});
});

// Publico images
app.get(/^\/images\/.+/,function(req,res){
    res.sendfile(req.url,{root:"static"});
});

// Publico sounds
app.get(/^\/sounds\/.+/,function(req,res){
    res.sendfile(req.url,{root:"static"});
});

// Publico index.html
app.get("/index.html",restrict,function(req,res){
    res.sendfile("index.html",{root:"static"});

    // Publico index.html
    app.get("/default",restrict,function(req,res){
        res.sendfile("index.html",{root:"static"});
    });

});


app.post('/login', function(req, res) {
    function loginSuccess() {
        req.session.regenerate(function() {
            // Store the user's primary key 
            // in the session store to be retrieved,
            // or in this case the entire user object
            req.session.user = req.body.usuario;
            res.cookie("usr",req.body.usuario);
            res.cookie("testing",req.body.usuario,{ maxAge: (24*3600*1000*30)});
            if (req.body.nombre != "")
                res.cookie("nombre",req.body.nombre);
            else
                res.cookie("nombre",req.body.usuario);
            
            if(req.cookies.redirect_to){
                res.redirect(req.cookies.redirect_to);
                res.clearCookie("redirect_to");
            }
            else
                res.sendfile('index.html', {root: './static'});
        });
    }

    function loginFailure(errText, errCode) {
        console.log("failed to login. "+errCode+": "+errText);
        res.redirect('/login');
    }
  
    checkPassword(req.body.usuario, req.body.server, req.body.password, loginSuccess, loginFailure);
});

// HANDLE ERRORS

// "app.router" positions our routes above the middleware defined below,
// this means that Express will attempt to match & call routes _before_ continuing
// on, at which point we assume it's a 404 because
// no route has handled the request.
//app.use(app.router);

app.use(function(req, res, next){
  res.status(404);

  // respond with html page
  if (req.accepts('html')) {
    res.sendfile('404.html',{root:"./static"});
    //res.render('404', { url: req.url });
    return;
  }

  // respond with json
  if (req.accepts('json')) {
    res.send({ error: 'Not found' });
    return;
  }

  // default to plain-text. send()
  res.type('txt').send('Not found');
});


/* ******************************************************************************************/
/* ******************************************************************************************/
/* ******************************************************************************************/
//                               Override easyrtc listeners
/* ******************************************************************************************/
/* ******************************************************************************************/
/* ******************************************************************************************/

/**
 * Sobreescribimos algunos eventos de easyrtc que neesitamos para cambiar el comportamiento <br>
 * <a href="http://www.easyrtc.com/docs/server/module-easyrtc_default_event_listeners-eventListener.html"> Documentacion oficial </a>
 * @class Override EasyRTC Listeners.
 */
//http://www.easyrtc.com/docs/server/module-easyrtc_default_event_listeners-eventListener.html

/**
 * Custom event. Lo utilizamos para comunicarnos cliente/servidor. Para los eventos que no estén en este handler, lo enviamos a l handler que está por default.
 * Así solo capturamos los que creamos necesarios.
 *
 * @event onEasyrtcMsg
 * @param {function} Handler para eventos enviados al servidor.
 * @example
        //En el cliente, podemos ejecutar este snippet para chequear si está poniendo bien los datos, y hacer el join o no , según me informe el server.     
        //easyrtc.sendServerMessage(msgType, data, successCB, failCB);
        easyrtc.sendServerMessage("entrarSala", { "password": password, "roomName": roomName }, 
                function(msgType, msgData){
                    console.log("msgType: " + msgType);
                    console.log("msgData: " + msgData);

                    if (msgType == "entrarSalaOk") {
                        easyrtc.joinRoom(roomName,null,successCB,failureCB);
                    } else{
                        popupAlert("No se puedo crear la sala, o intentó entrar a una sala con un password incorrecto.");
                    };
                }, 
                function(errorCode, errorText){
                    popupAlert("Se produjo un error al crear la sala, intente de nuevo.");
                }
    );

 */

// Creamos un handler para custom events. Para el resto, lanamos el default.
var onEasyrtcMsg = function(connectionObj, msg, socketCallback, next){
    switch (msg.msgType){
        /*case "message":
            console.log("Received message from " + connectionObj.getEasyrtcid(), msg);
            console.log("The message type is " + msg.msgType);
            console.log("The message data is " + msg.msgData.password);
            console.log("The message data is " + msg.msgData.roomName); 
            //return Ack to the client
            socketCallback({"msgType":"creoSalaOk"});
            break;*/

        case "entrarSala":
        // easyrtc.sendServerMessage("entrarSala", { "password": password, "roomName": roomName, "owner":owner, "queued":"no" , "create" : "", "expire" : "", "deleteOnStart": 0 }, 
            var res;
            var roomName = msg.msgData.roomName;
            var password = msg.msgData.password;
            /* *********************************************************/
            /* ********** Declaration of callbacks**********************/
            function loginSuccess() {
                console.log("[onEasyrtcMsg entrarSala - loginSuccess] Added Room " + roomName + "to the DB.");

                //Agregá este get
                if (password == "") 
                {
                    app.get('/'+roomName, function(req, res, next) {
                                res.cookie("roomName", roomName);
                                res.sendfile('/indexSalaPub.html',{root:"./static"});
                            });
                } else
                {
                    app.get('/'+roomName, function(req, res, next) {
                                res.cookie("roomName", roomName);
                                res.sendfile('/indexSala.html',{root:"./static"});
                            });    
                };
                
                res = ["entrarSalaOk", "Added Room " + roomName + " to the DB."];
                socketCallback({"msgType": res[0], "msgData": res[1]});
            }

            function loginFail(errText, errCode) {
                console.log("[onEasyrtcMsg entrarSala - loginFail] Failed to add room, " + errText);
                res = ["entrarSalaFallo", /*"Failed to add room, " + errText + "to the DB."*/ "Hubo un error de conexión, intente nuevamente"];
                deleteEntry(roomName);//para que no quede en una BD si y en otra no. => inconsistente
                socketCallback({"msgType": res[0], "msgData": res[1]});
            }
            function joinSuccess(roomName) {
                console.log("[onEasyrtcMsg entrarSala - joinSuccess] Password OK -> Joined room: " + roomName);
                res = ["entrarSalaOk", "Password OK -> Joined room: " + roomName];
                socketCallback({"msgType": res[0], "msgData": res[1]});
            }

            function joinFail(errText, errCode) {
                console.log("[onEasyrtcMsg entrarSala - joinFail] Failed to join room, try another password. " + errText);
                res = ["entrarSalaFallo", "Password incorrecto"];
                socketCallback({"msgType": res[0], "msgData": res[1]});
            }

            function gotEntry(entry) {
                //Si no existe la sala, la agregamos a la BD y agregamos el get para poder entrar via URL.
                //Si ya existe, tenemos que ver si es una sala que esta esperando para ser creada o no. (viendo el valor del campo queued)
                if (!entry){
                    //nombre, password, no encolada, owner, sin fecha de creacion, sin fecha eliminacion, no borrar on startup, sin emails invitados.
                    addRoom(roomName, password, "no" ,msg.msgData.owner, "" , "", 0,"", "yes","", loginSuccess, loginFail);
                }else if (entry.queued == "yes"){
                    console.log("[onEasyrtcMsg entrarSala - gotEntry] Room " + roomName + " already existed and  but it is queued for being created in the future, can not use that room name.");
                    res = ["entrarSalaFallo", "El nombre de reunión esta reservado, intente uno nuevo con otro nombre"];
                    socketCallback({"msgType": res[0], "msgData": res[1]});
                }else {
                    checkPasswordRoom(roomName, password, joinSuccess, joinFail);
                }
                //No les dejaba entrar a las que estan esperando para ser borradas, pero es lo mismo. Lo dejamos entrar, total en reseteo, chau, desaparecerá.
                /*else if (entry.queued == "no" && entry.deleteOnStartup == 0){ //No es una sala que esta esperando para ser creada o para ser borrada.
                    console.log("[onEasyrtcMsg entrarSala - gotEntry] Room " + roomName + " already existed and is not queued, we will check your password.");
                    console.log("[onEasyrtcMsg entrarSala - gotEntry] roomName = "+roomName + ", password = "+password);
                    checkPasswordRoom(roomName, password, joinSuccess, joinFail);
                }
                else {
                    console.log("[onEasyrtcMsg entrarSala - gotEntry] Room " + roomName + " already existed and  but it is queued for being created in the future, can not use that room name.");
                    res = ["entrarSalaFallo", "El nombre de reunión esta reservado, intente uno nuevo"];
                    socketCallback({"msgType": res[0], "msgData": res[1]});
                }*/
            }

            function failedEntry(errorMessage, errorCode) {
                console.log("[onEasyrtcMsg entrarSala - not entry] " + errorMessage);
                res = ["entrarSalaFallo", /*errorMessage*/ "Hubo un error de conexión, intente nuevamente"];
                socketCallback({"msgType": res[0], "msgData": res[1]});
            }

            //Start of execution, then "go up" through callbacks.
            getPasswordEntry(roomName, gotEntry, failedEntry);
            break;

        case "queueRoom":
            var roomName = msg.msgData.roomName;
            var password = msg.msgData.password;
            //Si se encoló la sala a la BD, enviamos mails a los invitados. Se van a mandar acá y tambien cuando se haya seteado en el campo cuando se programo la creacion de sala.
            function loginSuccessQ() {
                console.log("[onEasyrtcMsg queueRoom - loginSuccessQ] Added Room " + roomName + " to the DB. It will wait for being created on time.");
                if (msg.msgData.emailsInvitados != "") {
                    var datos = [];
                    var datosTemp=[];
                    datosTemp = [roomName, msg.msgData.owner, msg.msgData.create, msg.msgData.emailsInvitados];
                    datos.push(datosTemp);    
                    sendMailsOnQueued(datos);
                };
                socketCallback({"msgType": "queueRoomOk", "msgData": "La reunion " + roomName + " esperará para ser creada en el momento indicado."});
            }

            function loginFailQ(errText, errCode) {
                console.log("[onEasyrtcMsg entrarSala - loginFail] Failed to add room, " + errText);
                deleteEntry(roomName);
                socketCallback({"msgType": "queueRoomFail", "msgData": "Hubo un error de conexión, intente nuevamente"});
            }

            function gotEntryQueue(entry) {
                if (!entry){
                    //nombre, password, encolada, owner, fecha de creacion, fecha eliminacion, no borrar on startup, emails invitados.
                    addRoom(roomName, password, "yes" ,msg.msgData.owner,msg.msgData.create, msg.msgData.expire, 0, msg.msgData.emailsInvitados, "no", msg.msgData.fechaAviso, loginSuccessQ, loginFailQ);
                }
                else {
                    console.log("[onEasyrtcMsg queueRoom - gotEntry] Room " + roomName + " already exists, cannot .");
                    socketCallback({"msgType": "queueRoomFail", "msgData": "El nombre de reunión esta reservado, intente uno nuevo con otro nombre"});
                }
            }

            function failedEntryQueue(errorMessage, errorCode) {
                console.log("[onEasyrtcMsg queueRoom - failedEntry] " + errorMessage);
                socketCallback({"msgType": "queueRoomFail", "msgData": "Hubo un error de conexión, intente nuevamente"});
            }

            getPasswordEntry(roomName, gotEntryQueue, failedEntryQueue);
            break;
        case "isPasswordlessRoom": //Utilizamos este mensaje para saber si es una sala sin password (por ejemplo para no mostrar el popup de ingresar a sala)
            var roomName = msg.msgData.roomName;
            var password = "";

            function joinSuccessPassless(roomName) {
                console.log("[onEasyrtcMsg isPasswordlessRoom - joinSuccess] Es sala sin password : " + roomName);
                socketCallback({"msgType": "Ok", "msgData": "Es sala sin password: " + roomName});
            }

            function joinFailPasless(errText, errCode) {
                console.log("[onEasyrtcMsg isPasswordlessRoom - joinFail] Es sala con password : " + roomName);
                socketCallback({"msgType": "Fail", "msgData": "Fallo [onEasyrtcMsg isPasswordlessRoom] "+ errText});
            }

            checkPasswordRoom(roomName, password, joinSuccessPassless, joinFailPasless);
            break;

        case "deleteRoom":
            var roomName = msg.msgData.roomName;
            console.log("Por borrar la sala "+roomName);
            easyRtcAppObj.deleteRoom(roomName, function(err) {
                if (err) {
                    console.log("[onEasyrtcMsg deleteRoom - failDelete] No se pudo borrar la sala : " + roomName+" . Paso lo siguiente: " + err);
                    socketCallback({"msgType": "FailDelete", "msgData": "No se pudo borrar la sala. " + err});
                } else{
                    console.log("[onEasyrtcMsg deleteRoom - succesDelete] Se borró la sala : " + roomName);
                    deleteEntry(roomName); //Si no se llega a borrar, dsps cuando creen una sala, como se inserta con replace, no hay conflicto.
                    socketCallback({"msgType": "OkDelete", "msgData": "Se borro la sala "+ roomName+" exitosamente."});    
                };
            });
            break;
        case "isOwner":
            var res;
            var roomName = msg.msgData.roomName;
            var owner = msg.msgData.owner;
            console.log("[onEasyrtcMsg isOwner] About to check if "+owner+" is the owner of the room "+roomName);

            function gotEntryIsOwner(entry) {
                if (!entry)
                    socketCallback({"msgType": "FailIsOwner", "msgData": "No hay sala con ese nombre"});
                else if(entry.owner == owner)
                    socketCallback({"msgType": "OkIsOwner", "msgData": "Eres el propietario"});
                else
                    socketCallback({"msgType": "FailIsOwner", "msgData": "No eres el propietario"});
            }

            function failedEntryisOwner(errorMessage, errorCode) {
                console.log("[onEasyrtcMsg queueRoom - failedEntry] " + errorMessage);
                socketCallback({"msgType": "FailIsOwner", "msgData": "Hubo un error de conexión, intente nuevamente"});
            }

            getEntry(roomName, gotEntryIsOwner, failedEntryisOwner);
            break;
        default:
            // Revert to default handler if the message type is unknown
            connectionObj.events.emitDefault("easyrtcMsg", connectionObj, msg, socketCallback, next);
            break;
    }
};


easyrtc.events.on("easyrtcMsg", onEasyrtcMsg);

/**
 * Lanzado cuando hacemos el connect(). Mas precisamente en easyrtc.easyApp( ... ). <br>
 * Redefinimos este handler, para diferenciar si está queriendo entrar por URL a una sala (un link que te hayan pasado), 
 * ó desde la página principal. Si se entra desde la página principal, quiere decir que ya pasó la etapa de logueo, por lo
 * que no lo volvemos a pedir alguna autenticación (next(null)). Si entra por un link, deberá pasar una etapa de logueo, 
 * verificando los datos que guardamos en las credenciales.
 *
 * @event onAuthenticate
 * @param {function} Handler para eventos enviados al servidor.
 * @example
            //seteamos credenciales en connect() de conferenciasSala.js
             easyrtc.setCredential({ 
            "roomName": roomName,
            "userName" : uname,  
            "password" : pass, 
            });
 */

//Sobreescribimos el evento "athenticate", para ver si entró por URL a una sala ó al default.
//Se lanza cuando nos queremos conectar a una aplicación de easyApp.. o sea en nuestro caso, en el connect.
var onAuthenticate = function(socket, easyrtcid, appName, username, credential, easyrtcAuthMessage, next){

    if(credential==null) { 
        next(null);
        return; 
    }

    function joinSuccess(roomName) {
        console.log("[onAuthenticate] joinSuccess Exitoso!");
        next(null);
    }

    function joinFail(errText, errCode) {
        console.log("Autenticacion erronea de "+username);
        next(new easyrtc.util.ConnectionError("Password incorrecta. Intente nuevamente."));
        //next("Password incorrecta. Intente nuevamente.");
    }

    //Si están definidas las credenciales, es porque entramos a una sala por URL 
    //Fijarse q en el conferenciasSAla.js las definimos y en el otro no.
    if (credential.roomName) 
    {
        console.log("[onAuthenticate] roomName: " + credential.roomName);
        console.log("[onAuthenticate] password: " + credential.password);
        console.log("[onAuthenticate] SI credential");
        checkPasswordRoom(credential.roomName, credential.password, joinSuccess, joinFail);
    }
    else
    {
        console.log("[onAuthenticate] NO credential");
        next(null);
    }
}

easyrtc.events.on("authenticate", onAuthenticate);

/* ******************************************************************************************/
/* ******************************************************************************************/
/* ******************************************************************************************/
//                             Trabajos programados
/* ******************************************************************************************/
/* ******************************************************************************************/
/* ******************************************************************************************/


// create reusable transport method (opens pool of SMTP connections)
var smtpTransport = nodemailer.createTransport("SMTP",{
    service: "Gmail",
    auth: {
        user: "gonzalo.amadio@itecnis.com",
        pass: "Gonzalo1.2,"
    }
});
//A cada hora y un minuto, nos fijamos si tenemos que crear salas programadas
//cada 5 min chequeamos para crear salas programadas
//Poniendo */5 para ejecutar cada 5 minutos, no funciono. tuve que poner todos los nums.
var j = schedule.scheduleJob('0,5,10,15,20,25,30,35,40,45,50,55 * * * *', function(){
    createQueuedRoom();
});
//Todos los dias a las 00:01, borramos las salas programadas (como se borran al final del día, lo hacemos una sola vez a esto, total es lo mismo)
//Lo hacemos después de que se creen las salas programdas (están en el mismo minuto, pero se ejecuta la de arriba primero).
var deleteQRooms = schedule.scheduleJob('0,5,9,10,15,20,25,30,35,40,45,50,55 * * * *', function(){
    deleteQueuedRoom();
});
//cada 5 min chequeamos para mandar mails
//Poniendo */5 para ejecutar cada 5 minutos, no funciono. tuve que poner todos los nums.
var j = schedule.scheduleJob('1,6,11,16,21,26,31,36,41,46,51,56 * * * *', function(){
    sendInvitationMails();
});

/* ******************************************************************************************/
/* ******************************************************************************************/
/* ******************************************************************************************/
//                                 Start server
/* ******************************************************************************************/
/* ******************************************************************************************/
/* ******************************************************************************************/

// ======= original easyrtc stuff  ================= //

var options = {
    //OJO!! PONER FULL PATH A LOS CERTIFICADOS
    key:  fs.readFileSync("/opt/nodes/easyrtc/easyssl/certs/iconf.key"),
    cert: fs.readFileSync("/opt/nodes/easyrtc/easyssl/certs/iconf.crt") 
    //key:  fs.readFileSync("certs/iconf.key"),
    //cert: fs.readFileSync("certs/iconf.crt")  
}


// Start Express http server on port 4443
var webServer = https.createServer(options,app).listen(4443);

// Start Socket.io so it attaches itself to Express server
var socketServer = io.listen(webServer, {"log level": 0});

easyrtc.setOption("logLevel","debug");
easyrtc.setOption("demosEnable",false);


var easyRtcAppObj = null;
// Start EasyRTC server
//var rtc = easyrtc.listen(app, socketServer);
// Start EasyRTC server
var rtc = easyrtc.listen(app, socketServer, null, function(err, rtc){
    if (err) throw err;

    rtc.createApp("Transatlantica", null, function(err, appObj){
        if (err) throw err;
        //console.log(require('os').hostname());
        //guardamos el appObj, para poder crear rooms en otro lado.
        easyRtcAppObj = appObj;
        //Crear todas las salas que no están marcadas como para ser borradas.
        createPersistentRooms();
    });
});
