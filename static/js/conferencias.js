/**
 * Implementacion de la logica de la aplicacion hecha por nosotros. <br>
 * Utilizamos Modulo como un archivo. <br> Y a lo que le llama clases, 
 * solamente son funciones que las agrupamos por la funcionalidad, pero no pertenecen a una clase per se.
 *
 * @module Conferencias.js
 */

//1) Funciones de conexion y desconexion al servidor.
//2) Funciones de llamada y desconexion con usuarios.
//3) Funciones de Sala.
//4) Funciones de callback para cuando hay llamadas entrantes.
//5) Configuracion de estado y texto perosonalizado de usuario.
//6) Funciones auxiliares.
//7) Funciones de chat. 
//8) Funciones de file transfering.

/**
* Aqui en la documentacion no listamos todas las que hay, solo las mas importantes.
*
* @class Variables Globales
* 
*/

sglm[0]='';


var zindex = 0;             //variable global, para que caundo hacemos click en una ventana se aumente su z-index y pase al frente.
var selfEasyrtcid = "";     //Mi id.
var uname="";               //Mi nombre de usuario.
var umail="";               //Mi nombre de usuario de mail.
var smail="";               //Mi dominio de usuario de mail.
var owner="";               //Mi usuario de e-mail. Unico, para poner como owner en las salas.

/**
* Array con los id's de las ventanas donde se posicionaran 
* 
* @property videoIds
* @type {[string]}
*/
var videoIds = ["box1", "box2", "box3"];
/**
* Tenemos dos opciones de estar en las salas. Estar solo en la default (D), ó,  en la default y además otra sala(S). 
* Eso significa que nunca dejamos la sala default. <br>
* Si se da la primera situación , connectedToRoom = D. En la segunda situación, connectedToRoom = S. <br>
* Podemos utilizar esta variable para chequear la sala en la que está y además si esta conectado a una sola sala, o a dos.
* 
* @property connectedToRoom
* @type {string}
* @default "default"
*/
var connectedToRoom = "default";
/**
* Lista de salas creadas.
* 
* @property currentRoomList
* @type {Object}
*/   
var currentRoomList;
var maxCALLERS = 3;         //Maxima cantidad de usuarios en conferencia.
var talkingTo = null;
var cancelCall;
var incommingCallSound;
/**
* Estado del usuario. away(ausente),chat(no disponible),dnd(),xa(disponible)
* 
* @property currentShowState
* @type {string}
* @default "xa"
*/ 
var currentShowState = "xa"; //away(ausente),chat(no disponible),dnd(),xa(disponible)
/**
* Textos de estados del usuario. Disponible, No Disponible, "" .
* 
* @property currentShowText
* @type {Object}
* @default ""
*/ 
var currentShowText = "";
var roomUsers;
var roomDefault;
var idButtons = 0;

//Elementos de fondo.
var backgroundsFolder = "images/backgrounds/"
var backgrounds = ["b1.jpg","b2.jpg","b3.jpg","b4.jpg","b5.jpg","b6.jpg","b7.jpg"];
var currentBackground = 0;

easyrtc.onError = function(errorObject) { 
    console.log("Entre a onerror conferencias");
    popupAlert(errorObject.errorText);
    };

/**
* Cambia las imagenes de fondo. Se mueve circularmente por todas las imágenes.
* @method changeBackground
*/
function changeBackground() {
    currentBackground += 1;
    currentBackground %= backgrounds.length;
    //document.body.style.backgroundImage="url("+backgroundsFolder+backgrounds[currentBackground]+")";
    $('body').css("background-image", "url("+backgroundsFolder+backgrounds[currentBackground]+")");
}

//Oculta elemento por id.
function oculta(id) {
    //document.getElementById(id).visibility = "hidden";
    //document.getElementById(id).style.display = "none";
    $('#'+id).hide();
}

function muestra(id) {
    //document.getElementById(id).visibility = "visible";
    //document.getElementById(id).style.display = "block";
    $('#'+id).show();
}

//Desabilita elemento por id. Aparece de colore gris.
function disable(id) {
    //console.log("about to try disabling "  +id);
    document.getElementById(id).disabled = "disabled";
}

//Habilita elemento por id.
function enable(id) {
    //console.log("about to try enabling "  +id);
    document.getElementById(id).disabled = "";
}

///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
* Connect, callbacks de actualizaciones cuando hay cambios en las salas.
* @class Funciones de conexion y desconexion al servidor
* 
*/


//Conecta a la sala default del servidor.
$(document).ready(function(){

});

/**
* Conexion al principio, cuando se entra en la página (acceso al servicio).
* Se setean algunos callbacks necesarios, como por ejemplo el que escucha los cambios de usuarios en las salas a las que pertenecemos.
* @method connect
*/
function connect() {
    //Habilitamos debugueo en consola.
    //easyrtc.enableDebug(true);

    //Obtenemos nombre de usuario
    uname = getCookie("nombre");
    owner =  decodeURIComponent(getCookie("usr"));


    //Nombre de usuario debe tener caracteres y caracteristicas validas.
    if ( !easyrtc.setUsername(uname) ) {
        popupAlert("Nombre de usuario invalido");
        return false;
    }
    
   
    //Inicializacion de EasyRTC.
    easyrtc.easyApp(
        "Transatlantica", 
        "box0", //donde poner mi video
        videoIds, //donde poner los videos remotos
        loginSuccess, 
        loginFailure);

    easyrtc.enableDataChannels(true);

    //Funcion a la que se llama cuando cambia la lista de usuarios conectados.
    //Se registra el callback para averiguar quien mas (Ademas de self) esta conectado al server.
    easyrtc.setRoomOccupantListener(convertListToButtons);
    //Sets a listener for data sent from another client (either peer to peer or via websockets).
    easyrtc.setPeerListener(wasInvited, "invitation"); //wasInvited es el handler para el mensaje "invitation"
    //easyrtc.setPeerListener(acceptedInvitation, "acceptInvitation");
    easyrtc.setPeerListener(cancelledInvitation, "cancelInvitation");
    easyrtc.setPeerListener(addToConversation); //Todos los otros mensajes que no sean "invitation" o los otros dos.

    easyrtc.enableDataChannels(true);
    //Codigo para el aceptador de la llamada
    easyrtc.setAcceptChecker(acceptCheckP2P);

    easyrtc.setOnCall( function(easyrtcid, slot) {
        boxUsed[slot+1] = true;
        if(activeBox == 0 &&  easyrtc.getConnectionCount() == 1) { // first connection
            collapseToThumb();
        }
        document.getElementById(getIdOfBox(slot+1)).style.visibility = "visible";
        handleWindowResize();
    });


    easyrtc.setOnHangup(function(easyrtcid, slot) {
        boxUsed[slot+1] = false;
        console.log("hanging up on " + easyrtcid);
        if(activeBox > 0 && slot+1 == activeBox) {
            collapseToThumb();
        }
        setTimeout(function() {
            document.getElementById(getIdOfBox(slot+1)).style.visibility = "hidden";

            if( easyrtc.getConnectionCount() == 0 ) { // no more connections
                expandThumb(0);
            }
            handleWindowResize();
        },20);
        updatePresence();
        console.log("call with " + easyrtcid + "ended");
    });

    //Seteamos esto, asi cuando se conecta, se le ordenan bien los contactos.
    //updateConnectedToRoomAndStatus("default");
    //convertListToButtons ("default", roomDefault, null); //No hace falta, al cabiar el status, se llama ya.
}

/**
* Funcion a la que se llama si es correcto el logueo de easyApp.
*
* @method loginSuccess
* @param easyrtcId {easyrtcid} Seteamos este id como el nuestro. <br> selfEasyrtcid = easyrtcId; 
*/
function loginSuccess(easyrtcId) {
    selfEasyrtcid = easyrtcId;

    //Por defecto, nos conectamos a default.
    console.log("Conectado a default.");

    //Reordenamos la pantalla
    muestra("rightWrapperTopBar");
    muestra("userNameBlock");
    enable('otherClients');
    oculta("botonRoom");
    muestra('otherClients');
    muestra('salasDiv');
    muestra('labelConnectedUsers');
    oculta('loginBox');
    muestra('connectControls');
    oculta('creacionProgramadaSalaBox');
    document.getElementById("box0").style.visibility = "visible";
    
    startbcscroll();//Arrancamos el scroller de mensajes en el topbar
        // Prep for the top-down layout manager
    setReshaper('videos', reshapeFull);
    for(var i = 0; i < numVideoOBJS; i++) {
        prepVideoBox(i);
    }
    window.onresize = handleWindowResize;
    handleWindowResize(); //initial call of the top-down layout manager
    expandThumb(0);

    updateConnectedToRoomAndStatus("default");
    //Obtenemos la lista de salas convertRoomsToList, llama a la funcion argumentada con dicha data.
    easyrtc.getRoomList(convertRoomsToList, errorList);
    easyrtc.setDisconnectListener(function(){
       easyrtc.showError("SYSTEM-ERROR", "Se perdió la conexión al socket server");
    });
    //Habilitamos recepcion de archivos
    easyrtc_ft.buildFileReceiver(acceptRejectCB, blobAcceptor, receiveStatusCB);
}



//Funcion a la que se llama si es fallido el logueo de easyApp
function loginFailure(errorCode, message) {
    var reloadPage = setTimeout(function(){document.location.reload(false)},2000);
    console.log("Failed to connect. errorCode: "+errorCode+"\n message: "+message);
}

function errorList(errorCode, message) {
    console.log("Failed to list rooms. errorCode: "+errorCode+"\n message: "+message);
}

//Desconectarse del servidor.
function disconnect() {
    window.location.reload(false);
}




///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
* Funciones para llamarse entre usuarios, y colgar llamadas.
*
* @class Funciones de Llamada y Desconexion con Usuarios
* 
*/

/**
* Llamar a un usuario cortando previamente otras conexiones (para P2P).
* @method performCall
* @param otherEasyrtcid {easyrtcid} Id del usuario al que vamos a llamar.
*/
function performCall(otherEasyrtcid) {
    //Corto las llamadas actuales.
    easyrtc.hangupAll();
    talkingTo = null;

    //Armo y muestro el div de llamada saliente.
    document.getElementById("callingToLabel").innerHTML="Llamando a "+easyrtc.idToName(otherEasyrtcid);
    cancelCall = setTimeout(function(){document.getElementById("cancelCall").click()},30000);
    muestra("callingToBox");

    //updatePresence();
    updateConnectedToRoomAndStatus(connectedToRoom);
    performCallAux(otherEasyrtcid);
}

//Llamar a un usuario (argumento).
function performCallAux(otherEasyrtcid) {

    //Funcion que toma la respuesta.
    var acceptedCB = function(accepted, caller) {
        //Si la llamada fue p2p, cancelar el timeout y ocultar el box de llamada saliente.
        clearTimeout(cancelCall);
        oculta("callingToBox");

        //Llamada rechazada.
        if( !accepted ) {
            enable('otherClients');
        }
        //Llamada aceptada
        else{
            if(connectedToRoom == "default"){
                talkingTo = otherEasyrtcid;
                document.getElementById(talkingTo).className="connected";
                //Ponemos al llamante 1 en div principal.
                muestra("hangupButton");
                //Habilitamos el boton colgar
                enable('hangupButton');
            }
        }
    };

    //Realización de llamada exitosa.
    var successCB = function() {
        console.log("Llamando a "+easyrtc.idToName(otherEasyrtcid));
    };

    //Realización de llamada errónea.
    var failureCB = function() {
        enable('otherClients');
    };

    easyrtc.enableDataChannels(true);
    easyrtc.call(otherEasyrtcid, successCB, failureCB, acceptedCB);
}

/**
* Colgar todas las llamadas.
* @method hangup
*/
function hangup() {
    //estoy hablando con alguien, pero no en una sala.
    if (connectedToRoom == "default") {
        salirASalaDefault();
        return;
    };
    oculta("callingToBox");//Ocultamos la caja de llamada saliente
    easyrtc.hangupAll();//Cortamos todas las llamadas actuales.
    talkingTo=null;
    updatePresence();
    disable('hangupButton');
} 

///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
* Funciones que manejan como se ven reflejado, los cambios en las salas, en el usuario. <br>
* Son callbacks seteados en connect.
* @class Funciones de Sala
*/

/**
* Función seteaa como callback. <br>
* Funcion a la que se llama cada vez que hay algun cambio en la lista de usuarios logueados en alguna de las salas (a la que pertenezcamos). En data viene los datos de la sala que fue modificada. <br>
* En roomDefault y roomUsers vamos a tener guardada la data de la ultima configuracion de usuarios en la sala default y en una reunion respectivamente.<br>
* Para saber si esta conectado solo a la default, o, en la default y ademas una reunion chequeamos el valor de connectedToRoom.
*
* @method convertListToButtons
* @param roomName {string} Nombre de la sala, a la que pertenecemos, que se está acutalizando.
* @param data {obj} Datos de los otros usuarios, que no soy yo, que están en la sala roomName
* @param isPrimary
*/
function convertListToButtons (roomName, data, isPrimary) {

    if (roomName === null) {
        return;
    }

    if (roomName == "default") 
        roomDefault = data;
    else
        roomUsers = data;

    //Borramos todos los elementos en otherClients
    clearConnectList();

    //var otherClientDiv = document.getElementById('otherClients');
    var otherClientDef = document.getElementById('otherClientsDefault');
    var otherClientRoom =  document.getElementById('otherClientsRoom');

    //DEBEMOS MOSTRAR SIEMPRE LOS DE LA SALA DEFAULT, Y ADEMAS SI ESTAMOS EN UNA REUNION, MOSTRAR ESOS TAMBIEN.
    //
    //Si estoy conectado a una sala, muestro los integrantes de la sala.
    if (connectedToRoom != "default") 
    {   
        //creamos un titulo con el nombre de la sala
        var meetingName = document.createElement('h4');
        meetingName.className = "nombreSalas";
        meetingName.innerHTML = "Usuarios en: " + connectedToRoom;
        otherClientRoom.appendChild(meetingName);
        //ponemos los usuarios de la sala.
        var listElement = document.createElement('ul');
        listElement.className="users";
        otherClientRoom.appendChild(listElement);

        for(var i in roomUsers) {

            var listItem = document.createElement('li');
            listItem.innerHTML = easyrtc.idToName(i);
            listElement.appendChild(listItem);
            if(easyrtc.getSlotOfCaller(i) == -1 && i < selfEasyrtcid) {
                performCallAux(i);
            }
        }

    }; //FIN  if (connectedToRoom != "default") 

    //Para la sala default, muestro todos los usuarios. Esto lo hago siempre
    var meetingName = document.createElement('h4');
    meetingName.className = "nombreSalas";
    meetingName.innerHTML = "Usuarios Conectados";
    otherClientDef.appendChild(meetingName);

    var listElement = document.createElement('ul');
    listElement.className="users";
    otherClientDef.appendChild(listElement);

    for(var i in roomDefault) {
        var listItem = document.createElement('li');
        listItem.id=i;
        if(i == talkingTo){
            listItem.className="connected";
        } else {
            //away(ausente),chat(no disponible),dnd(),xa(disponible)
            switch(roomDefault[i].presence.show){
                case "away":
                    listItem.className = "away";
                    break;
                case "chat":
                    listItem.className = "busy";
                    break;
                default:
                    listItem.className = "";
                    break;
            }
        }
        listItem.innerHTML = easyrtc.idToName(i).substring(0,16)+(easyrtc.idToName(i).length > 16 ? "..." : "");
        //Si yo estoy conectado a solo default y el otro tambien, nos podemos llamar.
        if (connectedToRoom == "default" && roomDefault[i].presence.status == "default") {
        listItem.innerHTML +="<img src='images/call.png' class='links' onclick='performCall(\""+i+"\");'>";
        };
        //Si yo estoy en alguna sala, pongo el icono para invitar a sala. (para los que estan en mi sala no).
        if (connectedToRoom != "default" && roomDefault[i].presence.status != connectedToRoom /*roomDefault[i].presence.status == "default"*/) {
        listItem.innerHTML +="<img src='images/plus.png' class='links' onclick='inviteToRoom(\""+i+"\",\""+connectedToRoom+"\");'>";
        };
        //A todos les pongo el icono del chat.
        listItem.innerHTML +="<img src='images/chat.png' class='links' onclick='makeChatWindow(\""+i+"\");'>";
        listElement.appendChild(listItem);
    } //Fin del for para crear los usuarios conectados a la sala default

    //Nos fijamos que no haya cambiado la informacion sobre las salas.
    easyrtc.getRoomList(convertRoomsToList, null); //no sacar, sino no se ve bien la lista de salas cuando entramos.
    updateChatButtonsState();
}


/**
* Funcion para crear una sala. Si se quiere crear una sala que ya existe y no está esperando para ser creada, se conecta a la misma. Si, no, no hace nada.
* @method crearSala
*/
function crearSala() {
    //Obtener un nombre valido.
    var roomName=document.getElementById("salaName").value;
    var password=document.getElementById("salaPass").value;
    if (roomName.length==0) {
        popupAlert("Ingrese un nombre de sala");
        document.getElementById("salaName").focus();
        return false;
    }

    if (!roomName.match(/^[a-z0-9_.-]{1,32}$/gi)) 
    {
        roomName = roomName.replace(/[^a-z0-9\n\r]+/gi,"_");
        //roomName = roomName.replace(/ /g, "_");
        $("#salaName").val(roomName);
        document.getElementById("salaName").focus();
        return false;
    };

    //Limpiar el input.
    document.getElementById("salaName").value="";
    document.getElementById("salaPass").value="";

    //Si logro crear la sala, estare conectado a la misma. 
    var successCB = function() {

        //easyrtc.setRoomApiField(roomName,  "status", "activa");

        //Si no solo estamos en la default, cambiamos el nombre de connectedToRoom, a la sala a la que nos vamos a conectar.
        var onsuccess = function() {
            easyrtc.setAcceptChecker(acceptCheckMultiparty);
            updateConnectedToRoomAndStatus(roomName);


        };

        var onfailure = function() {
            popupAlert("No puede dejar la sala ".concat(connectedToRoom));
        };

        
        //Si estamos solamente conectados a la sala default
        if (connectedToRoom == "default") 
        {
            //Como solo estaremos en la sala default, guardamos el nombre de la sala a donde estamos conectados que no sea la sala default.
            updateConnectedToRoomAndStatus(roomName); 
            //Como vamos a estar en una reunion
            easyrtc.setAcceptChecker(acceptCheckMultiparty);
        } else
        {
            easyrtc.leaveRoom(connectedToRoom,onsuccess,onfailure);
        };

        document.getElementById("salaName").focus();
        easyrtc.getRoomList(convertRoomsToList, null); 
    };

    var failureCB = function() {
        popupAlert("Fallo al crear o entrar en la sala "+roomName);
        document.getElementById("salaName").focus();
        return;
    };


    //easyrtc.sendServerMessage(msgType, data, successCB, failCB);
    //Como no es sala programada, dejamos los campos vacios, y también ponemos 0 en deleteOnStart para que no se borre.
    //queued para indicar que NO es una sala que esta en la BD como programada para futuro.
    easyrtc.sendServerMessage("entrarSala", { "password": password, "roomName": roomName, "owner":owner }, 
                function(msgType, msgData){
                    //console.log("msgType: " + msgType);
                    //console.log("msgData: " + msgData);

                    if (msgType == "entrarSalaOk") {
                        easyrtc.joinRoom(roomName,null,successCB,failureCB);
                    } else{
                        popupAlert("Ups!, " + msgData);
                    };
                }, 
                function(errorCode, errorText){
                    popupAlert("Se produjo un error al entrar/crear la sala, intente de nuevo.");
                }
    );

    

    //Hacemos el join a la sala (con su nombre) que nos queremos conectar.
    //Si falla la creacion se ejecuta failureCB y si no falla successCB.
    //En successCB :  
    //a) Si estamos conectados a la default solamente, listo conectamos. Solo actualizamos la variable connectedToRoom para llevar la cuenta en que sala, que no es la default, estamos conectados
    //b) Si no solo estamos conectados a la default, dejamos la sala a la cual estemos conectados (ejecutando leaveRoom), para quedar en esta nueva sala creada y la default.
    //easyrtc.joinRoom(roomName,null,successCB,failureCB);
}//FIN funcion crearSala

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};
/**
* Validamos el campo donde pone mails, que sean 5 separado por comas, y que sean válidos.
* @method validEmailAddresses
* @param emails {string} String con emails separados por comas. Chequeamos si cumple eso.
*/
function validEmailAddresses(emails)
{
    if(emails == "")
        return true;
    var result = emails.split(",");
    if (result.length > 4)
        return false; 
    for(var i = 0;i < result.length;i++)
        if(!isValidEmailAddress(result[i])) 
            return false;           
    return true;
}
function crearSalaProgramada()
{
    $("#fechaCreacion").val("");
    $("#fechaVencimiento").val("");
    $("#tiempoAviso").val("");
    $("#salaProgramadaNombre").val("");
    $("#salaProgramadaPass").val("");
    $("#errorFechaCreacion").html("");
    $("#errorFechaBorrado").html("");
    $("#errorNombre").html("");
    $("#errorEmails").html(""); 
    $("#errorHora").html(""); 
    $("#emailArea").val("");
    muestra('creacionProgramadaSalaBox');

    document.getElementById("crearSalaProgramada").onclick = function()
    {
        var creDate = $("#fechaCreacion").val();
        var expDate = $("#fechaVencimiento").val();
        var avisar = $("#tiempoAviso").val();
        var roomName = $("#salaProgramadaNombre").val();
        var pass = $("#salaProgramadaPass").val(); 
        var emails = $("#emailArea").val();

        ////////////////VALIDACIONES//////////////////
        //////////////////////////////////////////////
        if (roomName == "") 
        {
            $("#errorNombre").html("Ingrese algún nombre de reunión.");
            return false;
        }else{
            $("#errorNombre").html("");
        }

        if (!roomName.match(/^[a-z0-9_.-]{1,32}$/gi)) 
        {
            roomName = roomName.replace(/[^a-z0-9\n\r]+/gi,"_");
            //roomName = roomName.replace(/ /g, "_");
            $("#salaProgramadaNombre").val(roomName);
            document.getElementById("salaProgramadaNombre").focus();
            return false;
        }

        if (creDate=="") 
        {
            $("#errorFechaCreacion").html("Ingrese la fecha en la que se creará la reunión.");
            return false;
        }else{
            $("#errorFechaCreacion").html("");
        }
        //Si la fecha de creacion es mayor a la de vencimiento, es decir, vence antes de ser creada, error!
        if (greaterThanDate(creDate,expDate)) 
        {
            $("#errorFechaBorrado").html("Ingrese una fecha posterior a la de creación.");
            return false;
        }else{
            $("#errorFechaBorrado").html("");
        }

        if( !validEmailAddresses( emails ) ) { 
            $("#errorEmails").html("Ingrese máximo 4 e-mails,separados por comas y sin espacios.");
            return false;
        }else{
            $("#errorEmails").html("");
        }

        if (avisar != "") {
            if (greaterThanDate(avisar,creDate)) 
            {
                $("#errorHora").html("Ingrese una fecha anterior a la de creación.");
                return false;
            }else{
                $("#errorHora").html("");
            }
        };
        ////////////////FIN VALIDACIONES//////////////////
        //////////////////////////////////////////////////
        easyrtc.sendServerMessage("queueRoom", { "password": pass, "roomName": roomName, "owner":owner,  "create" : creDate, "expire" : expDate, "emailsInvitados" : emails, "fechaAviso": avisar }, 
            function(msgType, msgData){
                if (msgType == "queueRoomOk") 
                {
                    oculta("creacionProgramadaSalaBox");
                    popupAlert(msgData);
                } 
                else
                {
                    oculta("creacionProgramadaSalaBox");

                    popupAlertCB("Ups!, " + msgData,
                        function(){
                            $("#salaProgramadaNombre").val("");
                            muestra('creacionProgramadaSalaBox');
                            $('#salaProgramadaNombre').focus();
                        }
                    );
                };//else
            }, //function
            function(errorCode, errorText){
         
                oculta("creacionProgramadaSalaBox");

                popupAlertCB("Se produjo un error al entrar a la sala, intente de nuevo.",
                    function(){
                       $("#salaProgramadaNombre").val("");
                        muestra('creacionProgramadaSalaBox');
                    }
                );
            }//function
        );//sendServerMessage
    }//Fin onclick

    document.getElementById("cancelarSalaProgramada").onclick = function()
    {
        $('#creacionProgramadaSalaBox').hide("fast");
    }
}


/**
* Función seteada como callback. Funcion a la que se llama cada vez que hay algun cambio en la lista salas. <br>
* Ademas de listar las salas. A cada elemento de la lista le asignamos una funcionalidad. Que cuando haga click sobre la sala, pueda entrar en la misma.  
*
* @method convertRoomsToList
* @param roomList {[string]} Lista con los nombres de las salas que hay creadas.
*/
function convertRoomsToList(roomList) {

    currentRoomList = roomList;
    var salasDiv = document.getElementById('salasChat');

    //Limpiamos la lista actual.
    clearSalasList();

    //Creamos la lista.
    var listElement = document.createElement('ul');
    listElement.className="rooms";
    salasDiv.appendChild(listElement);


    //Por cada sala disponible creamos un elemento de lista.
    for (var roomName in roomList) {

        if (roomName == "default") { continue;}

        var listItem = document.createElement('li');
        listItem.id = roomName;
        listItem.innerHTML = roomName.substring(0,14)+(roomName.length > 14 ? "..." : "");
        listItem.setAttribute("onmouseover","ShowToolTip(this.id)");

        //Para el resto de las salas (en las cuales no estamos), si hacemos click hace esto.
        //En la lista de salas, si hay mas de una, al hacer click en una sala que no es en la que estamos,
        //se conecta automaticamente a esa nueva sala y se desconecta de la que estabamos.
        listItem.onclick = function(roomName) {

            /*******************************/
            /***********Callbacks***********/
            var joinRoomSuccess = function() {
                var hb = document.getElementById("hangupButton");
                updateConnectedToRoomAndStatus(roomName);
                easyrtc.setAcceptChecker(acceptCheckMultiparty);
                enable("hangupButton");
                hb.onclick = function() { salirASalaDefault();};
                //convertRoomsToList(currentRoomList); //si pongo : easyrtc.getRoomList(convertRoomsToList, null);, loopea infinitamente.
            }

            var joinDefaultSuccess = function(){
                var hb = document.getElementById("hangupButton");
                updateConnectedToRoomAndStatus("default");
                easyrtc.setAcceptChecker(acceptCheckP2P);
                hb.onclick = function() { hangup(); };
                disable("hangupButton");
                easyrtc.getRoomList(convertRoomsToList, null); 
            }

            var joinFailure = function(){
                easyrtc.joinRoom("default",null,joinDefaultSuccess,null);
                easyrtc.setAcceptChecker(acceptCheckP2P);
            }

            var leaveRoomSuccess = function(){
                easyrtc.joinRoom(roomName,null,joinRoomSuccess,joinFailure);
            }

            var leaveRoomFailure = function(){
                popupAlert("Fallo al desconectar de ".concat(connectedToRoom));
            }
            /***********Fin Callbacks***********/
            /***********************************/

                      
         return function(){

            //Primero ejecutarmos isPasswordlessRoom.. si no tiene password, entramos directamente.
            //Si tiene password, pedimos el password con askRoompopup (que pide el password y lo va a chequear a la BD).

            function conPass(){
                function succ(){
                    if (connectedToRoom == "default") 
                    {
                        easyrtc.joinRoom(roomName,null,joinRoomSuccess,joinFailure);
                        updateConnectedToRoomAndStatus(roomName); 
                    } else
                    {
                        easyrtc.hangupAll();
                        easyrtc.leaveRoom(connectedToRoom,leaveRoomSuccess,leaveRoomFailure);
                    };
                }

                function fail(why){
                    /*if (why != "cancelled") {
                    askRoomPassPopup(roomName,succ,fail);
                    };*/ //Si pone mal el pass, que le aparezca de nuevo el cartel.
                }

                //Ejecutamos como callback, asi esperamos que se ingresen los datos.
                askRoomPassPopup(roomName,succ,fail);

            }//conPass

            function sinPass(){
                if (connectedToRoom == "default") 
                {
                    easyrtc.joinRoom(roomName,null,joinRoomSuccess,joinFailure);
                    updateConnectedToRoomAndStatus(roomName); 
                } else
                {
                    easyrtc.leaveRoom(connectedToRoom,leaveRoomSuccess,leaveRoomFailure);
                };
            }//sinPass

            isPasswordlessRoom(roomName,sinPass, conPass);
        };//FIN return function.


        }(roomName) //FIN listitem.onclick

        //Usar icono especial si estamos conectado a esta sala.
        if(roomName == connectedToRoom){
            listItem.className="connected";
            listItem.onclick = function(){};
            listItem.innerHTML +="<img src='images/exit.png' class='links' id='salirR' onclick='salirASalaDefault()'>";
            listItem.innerHTML +="<img src='images/link.png' class='links' id='getlink' onclick='getLink(\"" +roomName+ "\");'>";
            listItem.innerHTML +="<img src='images/delete.png' class='links' id='delR' onclick='deleteRoom(\"" +roomName+ "\");'>";
            listItem.innerHTML +="<img src='images/mail.png' class='links' id='enviarInvR' onclick='sendRoomMail(\"" +roomName+ "\");'>";
            listItem.innerHTML +="<img src='images/chat.png' class='links' id='chatR' onclick='makeChatWindow(\"" +roomName+ "\");'>";
            listElement.appendChild(listItem);
            //Mostramos cartelitos on mouse over
            document.getElementById("salirR").setAttribute("onmouseover","ShowToolTip(this.id)");
            document.getElementById("getlink").setAttribute("onmouseover","ShowToolTip(this.id)");
            document.getElementById("delR").setAttribute("onmouseover","ShowToolTip(this.id)");
            document.getElementById("enviarInvR").setAttribute("onmouseover","ShowToolTip(this.id)");
            document.getElementById("chatR").setAttribute("onmouseover","ShowToolTip(this.id)");
        }
            
        listElement.appendChild(listItem);

        if(connectedToRoom != "default")
            ;
        else
            oculta("salirDeSala");
    }//FIN for 

    updateChatButtonsState();
}//FIN funcion.

/**
* Borra una sala. Primero se fija si sos el dueño de la sala a borrar. Luego se fija que no haya nadie mas que uno en la sala.
* Luego te saca de la sala (ya que sino, no te deja borrarla, porque tiene que estar vacía.). Luego envia mensaje al server para que borre la sala.
* @method deleteRoom
* @param roomName {string} Nombre de la sala a borrar.
*/
function deleteRoom(roomName)
{
    //callback por si no es el dueño
    function fail( ){
        popupAlert("No eres el propietario de "+ roomName +". No puedes borrarla.");
    }
    //callback para cuando si es el dueño. Preguntamos si esta seguro de borrarla.
    function suc()
    {
        document.getElementById('acceptCallBox').style.display = "block";
        document.getElementById('acceptCallLabel').innerHTML = "Está seguro de que desea eliminar la sala "+roomName+"?";

        //Usuario oprime Aceptar
        document.getElementById("callAcceptButton").onclick = function() {

            document.getElementById('acceptCallBox').style.display = "none";

            async.series([
                function(callback){
                    if (easyrtc.getConnectionCount() > 0) {
                        //popupAlert("Hay gente en la reunión, no puedes borrarla.");
                        callback("Hay gente en la reunión, no puedes borrarla.");
                    } else{
                        salirASalaDefaultCB(callback);
                    }
                },
                function(callback){
                    easyrtc.sendServerMessage("deleteRoom", { "roomName": roomName}, 
                        function(msgType, msgData){
                            if (msgType == "OkDelete") {
                                callback(null);
                            }else{
                                callback(msgData);
                            }                    
                        }, 
                        function(errorCode, errorText){
                            popupAlert("Se produjo un error al borrar a la sala, intente de nuevo.");
                        }
                    );//serverMsg
                }
            ],
            // optional callback
            function(err, results){
                if(err)
                    popupAlert(err);
                else //Lo hacemos desde el server.
                    updateConnectedToRoomAndStatus("default");
            });
        }

        //Usuario oprime rechazar
        document.getElementById("callRejectButton").onclick =function() {
            document.getElementById('acceptCallBox').style.display = "none";
        }; 
    }

    isOwner(roomName,suc,fail);
}

function isOwner(roomName,successCB,failCB)
{
    easyrtc.sendServerMessage("isOwner", { "roomName": roomName, "owner" : owner}, 
                function(msgType, msgData){
                    if (msgType == "OkIsOwner")
                        successCB();
                    else
                        failCB();
                }, 
                function(errorCode, errorText){
                    popupAlert("Se produjo un error, intente de nuevo.");
                }
    );   
}

/**
* @method inviteToRoom
* @param otherEasyrtcId {string} Id del otro usuario que querramos invitar a la sala que estemos conectados
* @param roomName {string} Nombre de la sala a la que vamos a invitar al otro usuario. (Es la que estamos conectados)
*/
function inviteToRoom(otherEasyrtcId, roomName)
{
    //callback por si no es el dueño
    function fail(){
        popupAlert("No eres el propietario de "+ roomName +". No puedes invitar a otro usuario.");
    }

    function suc(){    
        //Muestro el cuadro de dialogo y reproduzco sonido.
        document.getElementById('acceptCallBox').style.display = "block";
        document.getElementById('acceptCallLabel').innerHTML = "Desea invitar a "+easyrtc.idToName(otherEasyrtcId)+" a la reunión "+roomName;

        //Usuario oprime Aceptar
        document.getElementById("callAcceptButton").onclick = function() {
            easyrtc.sendPeerMessage(otherEasyrtcId,"invitation",roomName);
             //Oculto el cuadro de dialogo para aceptar y rechazar.
            document.getElementById('acceptCallBox').style.display = "none";
        };
        
        //Usuario oprime rechazar
        document.getElementById("callRejectButton").onclick =function() { 
             //Oculto el cuadro de dialogo para aceptar y rechazar.
            document.getElementById('acceptCallBox').style.display = "none";
        };     
    }

    isOwner(roomName,suc,fail);
}

//AL FINAL AL PEDO, VOY A TENER QUE IR IGUAL A LA BD PARA BORRARLA, HAGO ESTE CHEQUEO AHÍ Y LISTO
////////////////
function isEmptyRoom(roomName,successCB,failCB)
{
    var flag = 0;
    //por las dudas chequeamos que estemos conectados a una sala, aunque no haria falta porque solo podes borrar una sala
    if (connectedToRoom != "default") 
    {
        for(var i in roomUsers)
            flag++;

        //if 0 == is empty => successCB
        (flag) ? failCB()  : successCB();
    }else {
        failCB();    
    }

    
}



/**
* Chequeamos si una sala tiene password. Si tiene llamamos a failCB, si no tiene a successCB.
* *
* @method isPasswordlessRoom
* @param roomName {string} Nombre de la sala  la cual queremos chequear si tiene password.
* @param successCB {function} Callback a quien llamaremos si la sala no tiene password.
* @param failCB {function} Callback a quien llamaremos si la sala tiene password.
*/
function isPasswordlessRoom(roomName,successCB,failCB)
{
     easyrtc.sendServerMessage("isPasswordlessRoom", { "roomName": roomName }, 
                function(msgType, msgData){
                    //Si la sala tiene pass vacio, entramos sin mostrar el logueo.
                    if (msgType == "Ok") {
                        successCB();
                    }else{
                        failCB();
                    }
                }, 
                function(errorCode, errorText){
                    popupAlert("Se produjo un error al entrar a la sala, intente de nuevo.");
                }
    );
}


/**
* Cuequeamos si la sala a la que queremos entrar tiene o no password. Si no tiene password, entramos sin más.
* Si la sala tiene password, mostramos una ventana donde deberá ingresar el password. <br>
* Ver que la llamamos en convertRoomsToList, (en realidad le asignamos esta funcionalidad a cada sala listada.)
*
* @method askRoomPassPopup
* @param roomName {string} Nombre de la sala a la cual nos queremos conectar
* @param successCB {function} Callback a quien llamaremos si el password coincide con el de la sala.
* @param failCB {function} Callback a quien llamaremos si el password no coincide con el de la sala.
*/

function askRoomPassPopup(roomName, successCB, failCB)
{

    /////////////////////////////////////////////////////////////////////////////
    //Si es una sala con password, mostramos el popup para que ingrese a la sala.
    document.getElementById('roomPassLabel').innerHTML = "Ingrese password de la reunión"
    document.getElementById('roomPopupPass').value="";

    $('#popupPrompts').show();
    $('#roomPopupPass').focus();
    
    document.getElementById("roomPassButtonOk").onclick = function()
    {
        var password=document.getElementById("roomPopupPass").value;

        //easyrtc.sendServerMessage(msgType, data, successCB, failCB);
        easyrtc.sendServerMessage("entrarSala", { "password": password, "roomName": roomName, "owner":owner}, 
                    function(msgType, msgData){
                        if (msgType == "entrarSalaOk") {
                            //Limpiar el input.
                            $('#popupPrompts').hide("fast");
                            successCB();
                        } else{
                            //failCB("fail");
                            //$('#roomPopupPass').val("Password Incorrecto");
                            $('#roomPopupPass').attr("placeholder", "Password Incorrecto");
                            $('#roomPopupPass').focus();
                        };

                        document.getElementById("roomPopupPass").value="";
                        
                    }, 
                    function(errorCode, errorText){
                        $('#popupPrompts').hide("fast");
                        popupAlertCB("Se produjo un error al entrar a la sala, intente de nuevo.",
                            function(){
                                $('#popupPrompts').show();
                            }
                        );
                    }
        );
    }//Fin onclick

    document.getElementById("roomPassButtonCancel").onclick = function()
    {
        $('#popupPrompts').hide("fast");
    }

}

/**
* @method salirASalaDefault
*/
function salirASalaDefault()
{
    if (connectedToRoom == "default") 
    {
        oculta("callingToBox");
        easyrtc.hangupAll();
        oculta('hangupButton');
        updateConnectedToRoomAndStatus("default");
    } else{
        var leaveRoomSuccess = function(){
            updateConnectedToRoomAndStatus("default");
            easyrtc.getRoomList(convertRoomsToList, null);  
            //Por ejemplo si estamos en sala "default y s1" y me voy de s1, quedando en la default solemante, no se me va a ejecutar
            //convertListToButtons, ya que en las salas en las que estoy (que ahora es solamente la default) no hubo cambios.
            //Por eso hacemos la llamada explicita.
            convertListToButtons("default", roomDefault, false);
            hangup();
            easyrtc.setAcceptChecker(acceptCheckP2P);
        }

        var leaveRoomFailure = function(){
                popupAlert("Fallo al desconectar de ".concat(connectedToRoom));
        }
        easyrtc.leaveRoom(connectedToRoom,leaveRoomSuccess,leaveRoomFailure);
    };
}

function salirASalaDefaultCB(cb)
{
    if (connectedToRoom == "default") 
    {
        oculta("callingToBox");
        easyrtc.hangupAll();
        oculta('hangupButton');
        updateConnectedToRoomAndStatus("default");
        cb(null);
    } else
    {
        var leaveRoomSuccess = function(){
            updateConnectedToRoomAndStatus("default");
            easyrtc.getRoomList(convertRoomsToList, null);  
            //Por ejemplo si estamos en sala "default y s1" y me voy de s1, quedando en la default solemante, no se me va a ejecutar
            //convertListToButtons, ya que en las salas en las que estoy (que ahora es solamente la default) no hubo cambios.
            //Por eso hacemos la llamada explicita.
            convertListToButtons("default", roomDefault, false);
            hangup();
            easyrtc.setAcceptChecker(acceptCheckP2P);
            cb(null);
        }

        var leaveRoomFailure = function(){
                popupAlert("Fallo al desconectar de ".concat(connectedToRoom));
                cb("Fallo al salir de la sala");
        }
        easyrtc.leaveRoom(connectedToRoom,leaveRoomSuccess,leaveRoomFailure);
    };
}

///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
*
* @class Funciones de callback  para cuando hay llamadas entrantes
* 
*/

/**
* Funcion callbacked cuando hay una llamada entrante. <br>  easyrtc.setAcceptChecker(acceptCheck);
* @method acceptCheck
* @param caller {easyrtcid} Id del llamante
* @param cb {function} Función callback que toma como parametro un bool según si se acepta o no la llamada.
*/

//Funcion callbacked cuando hay una llamada entrante.
function acceptCheckP2P(caller, cb) {
    //callerPending se usaba en el ejemplo original.
    callerPending = caller;

    //Muestro el cuadro de dialogo y reproduzco sonido.
    document.getElementById('acceptCallBox').style.display = "block";
    playIncommingCallSound();

    //Si ya estoy comunicado con alguien:
    if( easyrtc.getConnectionCount() > 0 ) {
        document.getElementById('acceptCallLabel').innerHTML = "Colgar llamada y aceptar nueva llamada de " +
            easyrtc.idToName(caller) + " ?";
    }
    //No estoy comunicado con nadie
    else {
        document.getElementById('acceptCallLabel').innerHTML = "Aceptar llamada de " + 
            easyrtc.idToName(caller) + " ?";
    }

    //Handler de la decision.
    var acceptTheCall = function(wasAccepted) {
        //Detengo sonido de llamada entrante
        stopIncommingCallSound();

       //Llamada aceptada.
        //Habilito boton para colgar.
        if(wasAccepted){
            muestra("hangupButton");
            enable('hangupButton');
            talkingTo = caller;
            document.getElementById(talkingTo).className="connected";
        }

        //Si ya estaba comunicado con otro usuario, cuelgo primero.
        if( wasAccepted && easyrtc.getConnectionCount() > 0 ) {
            easyrtc.hangupAll();
        }

        //Oculto el cuadro de dialogo para aceptar y rechazar llamadas.
        document.getElementById('acceptCallBox').style.display = "none";

        //Llamo a la funcion que se me paso para callback.
        cb(wasAccepted);

        //callerPending se usaba en el ejemplo original.
        callerPending = null;
    };

    //Usuario oprime Aceptar
    document.getElementById("callAcceptButton").onclick = function() {
        acceptTheCall(true);
    };
    
    //Usuario oprime rechazar
    document.getElementById("callRejectButton").onclick =function() {
        acceptTheCall(false);
    };            
}

//Funcion aceptadora de llamadas en modo multiparty. No pregunta, solo se fija si hay algun lugar de video libre
//Si hay libre, directamente hace helper(true) => acepta llamadas.
function acceptCheckMultiparty(caller, helper) {

    //Buscamos un lugar para la nueva llamada
    for (var i = 0; i < maxCALLERS; i++) {
        var video = getIthVideo(i);
        if (videoIsFree(video)) {
            helper(true);
            return;
        }
    }

    //No había lugar para la nueva llamada.
    helper(false);
}



///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
*
* @class Funciones de configuración de estado y texto personalizado de usuario
* 
*/

/**
* Actualiza el ESTADO del usuario.
* @method setPresence
* @param value {string} Puede ser uno de los tres valores : "Disponible", "No disponible", "Ausente"
*/
function setPresence(value) {
    //away(ausente),chat(no disponible),dnd(),xa(disponible)
    switch(value){
        case "Disponible":
            currentShowState = "xa";
            break;
        case "No disponible":
            currentShowState = "chat";
            break;
        default:
            currentShowState = "away";
            break
    }
    updatePresence();
}

/**
* Actualiza el TEXTO del usuario.
* @method updatePresenceStatus
* @param value {string} 
*/
function updatePresenceStatus(value) {
    currentShowText = value;
    updatePresence();
}

/**
* Actualiza el estado y el texto del usuario. Llama a convertListToButtons
* @method updatePresence
*/
function updatePresence()
{
    easyrtc.updatePresence(currentShowState, currentShowText);
}

/**
* Funcion que actualiza la sala a la cual está, y también el estado, para que cada vez que 
* se haga algún cambio, llame a convertListToButtons, y se actualize de manera correcta las listas de usuarios en cada sala.
*
* @method updateConnectedToRoomAndStatus
* @param room {string} Nombre de la sala a la cual estamos conectados. Seguimos la regla definida en la propiedad connectedToRoom.
*/
function updateConnectedToRoomAndStatus(room)
{
    connectedToRoom = room;
    //Me actualizo yo, y se le actualiza a los demás usuarios.
    updatePresenceStatus(room);

    //Ejecuto convertListToButtons para mi, asi se me actualizan bien a mi las listas.
    if (connectedToRoom == "default") 
    {
        convertListToButtons("default", roomDefault, true);
    } else
    {
        convertListToButtons("default", roomDefault, true);
        convertListToButtons(connectedToRoom, roomUsers, false);
    };

    //si nos conectamos a alguna sala (o sea que no vamos a chatear con alguien personalmente), escondemos el boton de llamada.
    updateChatButtonsState();
}

/**
* Actualiza el estado de los botones de la ventana de chat. Si nosotros estamos en reunion, no podremos llamar a nadie, estamos ocupados.
* Y si nosotros no estamos en ninguna reunión, podremos llamar al otro usuario dependiendo de si el está o no en una reunión.
*
* @method updateChatButtonsState
*/
function updateChatButtonsState()
{
    //Si yo estoy en una reunion, no puedo llamar a nadie.
    if (connectedToRoom != "default") 
    {
        $('.chatWindowButton_call').hide();
        muestra('botonRoom');
        muestra("hangupButton");
    }
    else //Si yo no estoy en ninguna reunion.
    {
        oculta('botonRoom');
        oculta("hangupButton");
        verUsrDefault();

        if (typeof roomDefault != 'undefined')
        {    //para todos los usuarios.
            for (var who in roomDefault) {
                //Existe ventana del usuario.
                if ((document.getElementById("chatWindowTopLevel"+who)!=null))
                {
                    //no esta en reunion.
                    if (roomDefault[who].presence.status == "default")
                        //lo puedo llamar.
                        $('#callChat'+who).show();
                    else//esta en una reunion.
                        //no lo puedo llamar.
                        $('#callChat'+who).hide();
                }
            }
        }
    }
}
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
*
* @class Funciones auxiliares.
* 
*/

/**
* Muestra un mensaje cuando pasamos el mouse por arriba de un elemento.
* @method ShowToolTip
* @param id {string} Id del elemento al que le queremos agregar un cartelito cuando pasamos el mouse por arriba
* @example  document.getElementById("salirR").setAttribute("onmouseover","ShowToolTip(this.id)");
*/
function ShowToolTip(id)
{
    var ToolTip = document.getElementById(id);
    switch(id)
    {
        case "salirR":
            ToolTip.title="Salir de la reunión";
            break;
        case "delR":
            ToolTip.title="Borrar reunión";
            break;
        case "enviarInvR":
            ToolTip.title="Enviar invitaciones por e-mail";
            break;
        case "chatR":
            ToolTip.title="Chatear con los integrantes de la reunión";
            break;
        case "getlink":
            ToolTip.title="Obtener link de la sala para compartir";
            break;
        default:
            ToolTip.title=id;
            break;
    }
}

/*function ShowToolTipLink(roomName)
{
    var ToolTip = document.getElementById("getlink");
    ToolTip.title=location.protocol+"//"+location.hostname+"/"+roomName;
}*/

function getLink(roomName)
{
    var msg = location.protocol+"//"+location.hostname+"/"+roomName;
    popupAlert("El link de la sala es: "+msg);
}

/**
* Muestra un mensaje estilo popup.
* @method popupAlert
* @param mensaje {string}
*/
function popupAlert(mensaje){
    document.getElementById('popupMessage').style.display = "block";
    document.getElementById('textToShowInPopup').innerHTML = mensaje;

    //Usuario oprime Aceptar
    document.getElementById("popupBoxButton").onclick = function() {
        document.getElementById('popupMessage').style.display = "none";
    };
}

/**
* Muestra un mensaje estilo popup y ejecuta callback.
* @method popupAlertCB
* @param mensaje {string}
* @param cb {function} Callback a ejecutar luego de ocultar el popup.
*/
function popupAlertCB(mensaje,cb){
    document.getElementById('popupMessage').style.display = "block";
    document.getElementById('textToShowInPopup').innerHTML = mensaje;

    //Usuario oprime Aceptar
    document.getElementById("popupBoxButton").onclick = function() {
        document.getElementById('popupMessage').style.display = "none";
        cb();
    };
}

//Para debuguear y ver los usuarios en la consola.
function imprimirUsuarios(roomName)
{
    if (roomName == "default") 
    {
        for(var i in roomDefault)
            console.log(easyrtc.idToName(i));
    } 
    else
    {
        for(var i in roomUsers)
            console.log(easyrtc.idToName(i));
    }
}

/**
* Elimina toda la informacion dentro de otherClients, o sea de la lista de los otros usuarios que vemos en las salas.
* @method clearConnectList
*/
function clearConnectList() {
/*    otherClientDiv = document.getElementById('otherClients');
    while (otherClientDiv.hasChildNodes()) {
        otherClientDiv.removeChild(otherClientDiv.lastChild);
    }
*/
    var otherClientDef = document.getElementById('otherClientsDefault');
    while (otherClientDef.hasChildNodes()) {
        otherClientDef.removeChild(otherClientDef.lastChild);
    }

    var otherClientRoom = document.getElementById('otherClientsRoom');
    while (otherClientRoom.hasChildNodes()) {
        otherClientRoom.removeChild(otherClientRoom.lastChild);
    }

}


/**
* Borrar la lista de salas.
* @method clearSalasList
*/
function clearSalasList() {
    salasChat = document.getElementById('salasChat');
    while (salasChat.hasChildNodes()) {
        salasChat.removeChild(salasChat.lastChild);
    }
}

function verUsrDefault(){
    muestra("otherClientsDefault");
    oculta("otherClientsRoom");
}

function verUsrSala(){
    oculta("otherClientsDefault");
    muestra("otherClientsRoom");
}

/**
* Predicado para saber si estamos o no conectados con otro usuario mediante video.
* @method notConnectedTo
* @param id {}
*/
function notConnectedTo(id){
    for (var i = 0; i < maxCALLERS; i++) {
        var video = getIthVideo(i);
        if (!videoIsFree(video)) {
            if(video.caller == id){
                return false;
            }
        }
    }
    return true;
}

/**
* Funcion que retorna el i-esimo video (de haber alguno)
* @method getIthVideo
* @param i {int}
* @return null ó div de video con id i
*/
function getIthVideo(i) {
    if (videoIds[i]) {
        return document.getElementById(videoIds[i]);
    }
    else {
        return null;
    }
}

/**
* Predicado que establece si un objeto de video esta libre.
* @method videoIsFree
* @param obj {obj}
* @return {bool}
*/
function videoIsFree(obj) {
    return (obj.caller === "" || obj.caller === null || obj.caller === undefined);
}

/**
* Funcion encargada de emitir sonido cuando hay una llamada entrante.
* @method playIncommingCallSound
*/
function playIncommingCallSound() {
    document.getElementById('incommingCall').play();
    incommingCallSound = setTimeout(playIncommingCallSound,50);
}

/**
* Funcion que detiene la reproduccion de un sonido.
* @method stopIncommingCallSound
*/
function stopIncommingCallSound() {
    clearTimeout(incommingCallSound);
}

/**
* Nos dice si el parámetro pasado es un easyrtcid válido (fijandonos si esta en alguna sala).
* @method isValidEasyrtcid
* @param who Id que queremos buscar
* @param roomData Sala donde lo queremos buscar
* @return {bool}
*/
function isValidEasyrtcid(who, roomData)
{
    var res = false;
    for(var i in roomData){
        if(i == who)
            res = true;
    }
    return res;
}

/**
* Nos dice si el parametro pasado nombre de sala válido.
* @method isValidRoomName
* @param who Nombre de la sala que queremos ver si existe.
* @return {bool}
*/
function isValidRoomName(who)
{
    var res = false;
    for(var i in currentRoomList){
        if(i == who)
            res = true;
    }
    return res;
}

/**
* Funcíon que compara dos fechas. Las fechas son tomadas del datepicker, y tienen el formato: "yyyy/mm/dd hh:mm"
* El segundo parametro vacío lo tomamos como que es nada. Si comparamos con nada, es menor.
* @method greaterThanDate
* @param d1 {string} String representing a date, with format "yyyy/mm/dd hh:mm"
* @param d2 {string} String representing a date, with format "yyyy/mm/dd hh:mm". If empty, d2 > d1.
*/
function greaterThanDate(d1,d2){

    //Si lo comparamos con nada, consideramos que es menor.
    if (d2=="") {
        return false;
    };

    var partsd1 = d1.split(' '); //partsd1[0] = "yyy/mm/dd", partsd2[1] = "hh:mm"
    var partsd2 = d2.split(' ');

    var dd1 = partsd1[0].split('/'); //dd1[0] = "yyyy", dd1[1] = "mm", dd1[2] = "dd"
    var dd2 = partsd2[0].split('/');

    var hd1 = partsd1[1].split(':'); //hd1[0] = "hh", hd1[1] = "mm"
    var hd2 = partsd2[1].split(':');
    //var d = new Date(year, month, day, hours, minutes, seconds, milliseconds);
    var date1 = new Date(dd1[0],dd1[1],dd1[2],hd1[0],hd1[1]);
    var date2 = new Date(dd2[0],dd2[1],dd2[2],hd2[0],hd2[1]);

    //console.log("Date1="+date1);
    //console.log("Date2="+date2);

    var res = (date1 > date2) ?  true : false;
    return res;
}


function sendRoomMail(roomName)
{
    var body = "Hola, %0D%0A Le envio la siguiente direccion para que pueda entrar en la reunión a la que le estoy invitando.%0D%0A %0D%0A "+ location.protocol+"//"+location.hostname+"/"+roomName;

    var link = 'mailto:?subject=Message from '
             + owner
             +'&body='+body;
    //window.location.href = link;
    window.open(link);
}

/**
* Maneja los mensajes que se muestran en el scroller. Llamada por addToConversation().
* @method addToScroller
* @param msg {string} mensaje que se va a agregar. Se saca el ultimo. (mensajes mostrados = 3).
*/
function addToScroller(msg)
{
    //solo la 1era vez de todo entra acá. Sino imprime undefined.
    if(sglm[0]=='')
        sglm.shift();

    if(sglm.length < 4)
        sglm.push(msg);
    else{
        sglm.shift(); //sacamos el 1 elemento
        sglm.push(msg); //ponemos el elemento en ultimo lugar   
    }
}

var originalTitle = document.title;
var blinkTitle;
var blinkLogicState = false;

function StartBlinking(title){
    //StopBlinking();
    blinkTitle = title; 
    BlinkIteration();
}

function BlinkIteration(){
    if(blinkLogicState == false)
        document.title = blinkTitle;
    else
        document.title = originalTitle;
    
    blinkLogicState = !blinkLogicState;
    blinkHandler = setTimeout(BlinkIteration, 1000);
}
    
function StopBlinking(){
    if(blinkHandler)
        clearTimeout(blinkHandler);
    
    document.title = originalTitle;
}
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///FUNCIONES AUXILIARES DEL EJEMPLO ORIGINAL. LA MAYORIA SOBREESCRITAS POR easyrtc.easyApp

var callerPending = null;

/*
easyrtc.setStreamAcceptor( function(caller, stream) {
    setUpMirror();
    var video = document.getElementById('callerVideo1');
    easyrtc.setVideoObjectSrc(video,stream);
    console.log("saw video from " + caller);
    enable("hangupButton");
});

easyrtc.setOnStreamClosed( function (caller) {
    easyrtc.setVideoObjectSrc(document.getElementById('callerVideo1'), "");
    if(connectedToRoom == "default")
        disable("hangupButton");
});
*/
easyrtc.setCallCancelled( function(caller){
    stopIncommingCallSound();
    if(connectedToRoom == "default")
        disable("hangupButton");
    if( caller === callerPending) {
        document.getElementById('acceptCallBox').style.display = "none";
        callerPending = false;
    }
});


//????????????????????????????
function setUpMirror() {
    //~ if( !haveSelfVideo) {
        //~ var selfVideo = document.getElementById("selfVideo");
        //~ easyrtc.setVideoObjectSrc(selfVideo, easyrtc.getLocalStream());
        //~ selfVideo.muted = true;
        //~ haveSelfVideo = true;
    //~ }    
}

///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
*
* @class Funciones de chat
* 
*/

/**
* Creamos una ventana de chat. Si es con otro usuario se creará con el id del usuario. Si es con una sala, con el nombre de la sala. Así se podrá distinguir entre ventanas, cual pertenece a que conversación.<br>
* Creamos dos versiones de ventanas, una grande y otra chiquita. Cuando minimizamos o maximizamos, hacemos hide de una y hose de la otra.
* @method makeChatWindow
* @param who {string} En este parametro vamos a tener el easyrtcid de un usuario si estamos chateando de manera personal con el. O el nombre de una sala, si es un chat de la misma.
*/
function makeChatWindow(who) {

    //Si ya existe, la maximizamos y listo, no la volvemos a crear.
    if (document.getElementById("chatWindowTopLevel"+who)!=null) {
        muestra("chatWindow"+who);
        oculta("chatWindowMin"+who);
        //document.getElementById("chatText"+who).focus();
        return;
    }

    var chatWindows = document.getElementById("chatWindows");

    var chatWindowTopLevel = document.createElement("div");
    chatWindowTopLevel.id = "chatWindowTopLevel"+who;
    chatWindowTopLevel.className = "chatWindowTopLevel";
    chatWindowTopLevel.onclick = function () {document.getElementById("chatWindowTopLevel"+who).style.zIndex = zindex+1;};

    var chatWindow = document.createElement("div");
    chatWindow.id="chatWindow"+who;
    chatWindow.className = "chatWindow";
    chatWindow.style.display="block";

    var chatWindowTopBar = document.createElement("div");
    chatWindowTopBar.id = "chatWindowTopBar";
    chatWindowTopBar.className = "chatWindowTopBar";

    var chattingWith = document.createElement("label");
    chattingWith.id = "chattingWith"+who;
    chattingWith.innerHTML = easyrtc.idToName(who);
    chattingWith.className = "chattingWith";

    var closeChatBut = document.createElement("img");
    closeChatBut.src="images/cerrar.png";
    closeChatBut.onclick = function () {closeChat(who);};
    closeChatBut.className= "chatWindowButton";
    closeChatBut.id="closeChat"+who;

    chatWindowTopBar.appendChild(chattingWith);
    chatWindowTopBar.appendChild(closeChatBut);

    //Si no es un nombre de sala, o sea, que es una ventana de algun usuario.
    if(!(isValidRoomName(who))){
        var callChatBut = document.createElement("img");
        callChatBut.src="images/llamar.png";
        callChatBut.onclick = function () {performCall(who);};
        callChatBut.className= "chatWindowButton_call";
        callChatBut.id="callChat"+who;

        chatWindowTopBar.appendChild(callChatBut);

        var sendFileBut = document.createElement("img");
        sendFileBut.src="images/file.png";
        sendFileBut.className= "chatWindowButton";
        sendFileBut.id="sendFileBut"+who;

        var sendFileInput = document.createElement("input");
        sendFileInput.type="file";
        sendFileInput.id="sendFileInput"+who;
        sendFileInput.name="sendFileInput"+who;
        sendFileInput.style.display="none";
        sendFileInput.multiple="true";

        chatWindowTopBar.appendChild(sendFileBut);
        chatWindowTopBar.appendChild(sendFileInput);

        updateChatButtonsState();

        $(sendFileBut).click(function () {
            $(sendFileInput).trigger('click');
        });
    }

    var minimizeChat = document.createElement("img");
    minimizeChat.src="images/minimizar.png";
    minimizeChat.onclick = function () {toggleChat(who);};
    minimizeChat.className= "chatWindowButton";
    minimizeChat.id="minimizeChat"+who;
    chatWindowTopBar.appendChild(minimizeChat);

    chatWindow.appendChild(chatWindowTopBar);

    var chatWindowText = document.createElement("div");
    chatWindowText.id = "chatWindowText"+who;
    chatWindowText.className = "chatWindowText";

    //CODIGO PARA FILETRANSFER//
    //Por ahora solo en conexciones p2p
    if(connectedToRoom == "default"){
        //Por ahora todo en console.log
        function updateStatusDiv(state) {
            console.log(state);
            switch (state.status) {
                case "waiting":
                    addToConversation(who,"1","Esperando a que el usuario acepte transferencia de archivos.");
                    //console.log("waiting for other party<br\>to accept transmission");
                    //statusDiv.innerHTML = "waiting for other party<br\>to accept transmission";
                    break;
                case "started_file":
                    console.log("started file: " + state.name);
                    //statusDiv.innerHTML = "started file: " + state.name;
                case "working":
                    console.log(state.name + ":" + state.position + "/" + state.size + "(" + state.numFiles + " files)");
                    //statusDiv.innerHTML = state.name + ":" + state.position + "/" + state.size + "(" + state.numFiles + " files)";
                    break;
                case "cancelled":
                    addToConversation(who,"1","Transferencia de archivos cancelada.");
                    console.log("cancelled");
                    // statusDiv.innerHTML = "cancelled";
                    // setTimeout(function() {
                    //     statusDiv.innerHTML = "";
                    // }, 2000);
                    break;
                case "done":
                    addToConversation(who,"1","Finalizó la transferencia de archivos.");
                    console.log("done");
                    // setTimeout(function() {
                    //     statusDiv.innerHTML = "";
                    // }, 3000);
                    break;
            }
            return true;
        }


        var noDCs = {}; // which users don't support data channels

        var fileSender = null;
        function filesHandler(files) {
            if (!fileSender) {
                fileSender = easyrtc_ft.buildFileSender(who, updateStatusDiv);
            }
            fileSender(files, true /* assume binary */);
        }
        easyrtc_ft.buildDragNDropRegion(chatWindowText, filesHandler);
        sendFileInput.onchange = function(e){
            if(e.srcElement.files.length>0)
                filesHandler(e.srcElement.files)
        };
    }



    chatWindow.appendChild(chatWindowText);

    var chatWindowInput = document.createElement("div");
    chatWindowInput.id = "chatWindowInput";
    chatWindowInput.className = "chatWindowInput";
    chatWindowInput.onclick = function () {StopBlinking();};

    var chatText = document.createElement("input");
    chatText.id = "chatText" + who;
    chatText.onkeydown = function(event){if (event.keyCode == 13) document.getElementById('sendChat'+who).click()};
    chatText.className = "chatText tawk_password_field";

    var sendChat = document.createElement("button");
    sendChat.id = "sendChat" + who;
    sendChat.className = "sendChat tawk_button";
    sendChat.innerHTML = "Enviar";
    sendChat.onclick = function() {sendChatText(who);};

    chatWindowInput.appendChild(chatText);
    chatWindowInput.appendChild(sendChat);

    chatWindow.appendChild(chatWindowInput);

    /////////////////////////////////////////////
    ////// Versión minimizada de la ventana /////
    var chatWindowMin = document.createElement("div");
    chatWindowMin.id = "chatWindowMin"+who;
    chatWindowMin.className = "chatWindowMin";

    var chatWindowMinBar = document.createElement("div");
    chatWindowMinBar.id = "chatWindowMinBar"+who;
    chatWindowMinBar.className = "chatWindowMinBar";

    var chattingWithMin = document.createElement("label");
    chattingWithMin.id = "chattingWithMin"+who;
    chattingWithMin.className = "chattingWithMin";

    var closeChatMin = document.createElement("img");
    closeChatMin.id = "closeChatMin" + who;
    closeChatMin.className = "closeChatMin chatWindowButton";
    closeChatMin.src = "images/cerrar.png";
    closeChatMin.onclick = function () {closeChat(who);};
    
    var maximizeChat = document.createElement("img");
    maximizeChat.id = "maximizeChat" + who;
    maximizeChat.className = "maximizeChat chatWindowButton";
    maximizeChat.src = "images/maximizar.png";
    maximizeChat.onclick = function () {toggleChat(who);};

    chatWindowMinBar.appendChild(chattingWithMin);
    chatWindowMinBar.appendChild(closeChatMin);
    chatWindowMinBar.appendChild(maximizeChat);

    chatWindowMin.appendChild(chatWindowMinBar);

    chatWindowTopLevel.appendChild(chatWindow);
    chatWindowTopLevel.appendChild(chatWindowMin);

    chatWindows.appendChild(chatWindowTopLevel);
    chatText.focus();

    $(".chatWindow").draggable({ handle : ".chatWindowTopBar"});    
    $(".chatWindowMin").draggable({ handle : ".chatWindowTopBarMin"});

    //si estoy en una reunion, escondo el icono de llamada.
    updateChatButtonsState();

}

/**
* "Cambia" una ventana de maximizada a minimizada y viceversa. Tenemos creados los dos elementos, y muestra uno o el otro según corresponda. Es decir, si esta visible el elemento ventana maximizada,
* lo oculta y muestra el elemento ventana minimizada y viceversa.
* 
* @method toggleChat
* @param who {string} Id de la ventana que queremos maximizar o minimizar.
*/
function toggleChat(who) {
    if(document.getElementById("chatWindow"+who).style.display == "block"){
        oculta("chatWindow"+who);
        muestra("chatWindowMin"+who);
        document.getElementById("chattingWithMin"+who).innerHTML = document.getElementById("chattingWith"+who).innerHTML;
        document.getElementById("chatWindowMin"+who).style.left = document.getElementById("chatWindow"+who).style.left;
        document.getElementById("chatWindowMin"+who).style.bottom = document.getElementById("chatWindow"+who).style.bottom;
    } else {
        muestra("chatWindow"+who);
        oculta("chatWindowMin"+who);
        document.getElementById("chattingWith"+who).innerHTML = document.getElementById("chattingWithMin"+who).innerHTML;
        document.getElementById("chatWindow"+who).style.left = document.getElementById("chatWindowMin"+who).style.left;
        document.getElementById("chatWindow"+who).style.bottom = document.getElementById("chatWindowMin"+who).style.bottom;
    }
}

/**
* @method closeChat
* @param who {string} Id de la ventana que queremos cerrar.
*/
function closeChat(who) {
    //oculta("chatWindowTopLevel"+who);
    oculta("chatWindow"+who);
    oculta("chatWindowMin"+who)
}

/**
* Funcion seteada como callback para cuando nos llega un chat. La seteamos en connect(): easyrtc.setPeerListener(addToConversation); <br>
* Como solo se llama sola, cuando nos llega un mensaje, la vamos a llamar nosotros cuando enviamos un mensaje y poner el mensaje enviado en la ventana correspondiente. Y así ver lo que nosotro escribimos, ya que si no la llamamos, solo veríamos los mensajes de los otros.
* Para poder distinguir en que ventana poner el texto, utilizamos el parametro who. <br>
* <!-- El parametro who va a ser : Si msgType="1", who=toWhom|roomName. Si msgType = "2", who=toWhom. Si msgType="3", who=roomName (que va a ser igual a connectedToRoom) -->
*
* @method addToConversation
* @param who {string} Id de la ventana a la que queremos agregar el texto enviado.
* @param msgType {string} Puede tomar valores "1", "2", "3", "Warning". según si es un texto que enviamos nosotros, nos enviaron p2p, nos enviaron multiparty, o un error respectivamente.
* @param content Texto que enviamos o recibimos.
*/
function addToConversation(who, msgType, content) {
    
    var quienB = document.createElement("b");
    var quien;
    // Escape html special characters, then add linefeeds.
    content = content.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;");
    content = content.replace(/\n/g, "<br />");
    var que = document.createTextNode(content);
    var br = document.createElement("br");
    var objDiv;
    var scrollMessage = false;

    switch(msgType){
        //Me envie a mi mismo un mensaje.
        case "1":
            //Ventana donde mande el mensaje (identificada con el id o el nombre de la sala)
            makeChatWindow(who);
            objDiv = document.getElementById("chatWindowText"+who);//who es id|roomName
            quien = document.createTextNode("Yo: ");
            break;

        //Me llego un mensaje p2p
        case "2":
            //Ventana de chat con el id de quien me mando el mensaje.
            makeChatWindow(who);
            objDiv = document.getElementById("chatWindowText"+who);
            quien = document.createTextNode(easyrtc.idToName(who)+": ");
            scrollMessage = true;
            StartBlinking(content);
            break;

        //Me llego un msj de sala   
        case "3":
            //Ventana de chat con el nombre de la sala.
            makeChatWindow(connectedToRoom);
            objDiv = document.getElementById("chatWindowText"+connectedToRoom);
            quien = document.createTextNode(easyrtc.idToName(who)+": ");
            scrollMessage = true;
            StartBlinking(content);
            break;

        //Error 
        case "Warning":
            //Ventana donde mande el mensaje (identificada con el id o el nombre de la sala)
            makeChatWindow(who);
            objDiv = document.getElementById("chatWindowText"+who);
            quien = document.createTextNode("Admin: ");
            break;
    }//switch

    quienB.appendChild(quien);
    objDiv.appendChild(quienB);
    objDiv.appendChild(que);
    objDiv.appendChild(br);
    objDiv.scrollTop = objDiv.scrollHeight;

    if(scrollMessage)
        addToScroller(easyrtc.idToName(who)+": "+content);

    document.getElementById('chatSound').play();
}//function

/**
* Envia el texto ingresado a quien corresponda, y además llama a addToConversation para enviarnos a nosotros mismos y ver reflejado en nuestras ventanas lo que escribimos.
*
* @method sendChatText
* @param toWhom {string} Toma uno de los dos valores: easyrtcid ó nombre de la sala,  si estamos chateando p2p, o si es un chat multiparty.
*/
function sendChatText(toWhom) {
    var sent=0;
    var text = document.getElementById("chatText"+toWhom).value;
    
    if (text != "" && text){
        document.getElementById("chatText"+toWhom).value = "";

        if (isValidEasyrtcid(toWhom, roomDefault) || isValidEasyrtcid(toWhom,roomUsers)) 
        {
            addToConversation(toWhom,"1",text);//"Me envio" a mi mismo el mensaje, con el id del usuario con el que estoy chateando.
            easyrtc.sendPeerMessage(toWhom,"2",text);//Envio el mensaje al otro usuario, p2p (msgType="2").
        } 
        else if (isValidRoomName(toWhom))
        {
            if(toWhom != connectedToRoom)
                addToConversation(toWhom,"Warning","No estas conectado a esta sala.");
            else
            {
                addToConversation(connectedToRoom,"1",text); //"Me envio" a mi mismo el texto. Y como es un chat de sala, me envio el nombre de la sala donde irá el mensaje.
                for(var i in roomUsers) //Le envio el texto a todos los integrantes de la sala, tipo multiparty (msgType = "3").
                    easyrtc.sendPeerMessage(i,"3",text);
            }
        } 
        else
        {
            addToConversation(toWhom,"Warning","Conexion no disponible.");
        }
    }//if
    
    document.getElementById("chatText"+toWhom).focus();
}

///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
*
* @class Peer Listeners
* 
*/
//easyrtc.setPeerListener(wasInvited, "invitation"); //wasInvited es el handler para el mensaje "inviting"
//easyrtc.setPeerListener(acceptedInvitation, "acceptInvitation");
//easyrtc.setPeerListener(cancelledInvitation, "cancelInvitation");

/**
* Envia el texto ingresado a quien corresponda, y además llama a addToConversation para enviarnos a nosotros mismos y ver reflejado en nuestras ventanas lo que escribimos.
*
* @method wasInvited
* @param who {string} easyrtcid de quien te manda la invitación.
* @param msgType {string} Tipo de mensaje enviado (En el código no aparece xq este es el listener seteado ya para un tipo de mensaje).
* @param who {roomName} Sala a la que estas siendo invitado.
*/
function wasInvited(who, msgType, roomName) 
{
    //Muestro el cuadro de dialogo y reproduzco sonido.
    document.getElementById('acceptCallBox').style.display = "block";
    playIncommingCallSound();

    //Si ya estoy comunicado con alguien: (No debería pasar nunca, ya que solo puedo invitar a alguien que no está en reunión.)
    if( easyrtc.getConnectionCount() > 0 ) {
        document.getElementById('acceptCallLabel').innerHTML = "Estas siendo invitado por "+easyrtc.idToName(who)+" a la reunión "+roomName+". Desea colgar la llamada actual y entrar?";
    }
    //No estoy comunicado con nadie
    else {
        document.getElementById('acceptCallLabel').innerHTML = "Estas siendo invitado por "+easyrtc.idToName(who)+" a la reunión "+roomName;
    }


    //Usuario oprime Aceptar
    document.getElementById("callAcceptButton").onclick = function() {

            /*******************************/
            /***********Callbacks***********/
            var joinRoomSuccess = function() {
                var hb = document.getElementById("hangupButton");
                updateConnectedToRoomAndStatus(roomName);
                easyrtc.setAcceptChecker(acceptCheckMultiparty);
                enable("hangupButton");
                hb.onclick = function() { salirASalaDefault();};
                //easyrtc.sendPeerMessage(who, "acceptInvitation", "Invitacion aceptada.");   
            }

            var joinDefaultSuccess = function(){
                var hb = document.getElementById("hangupButton");
                updateConnectedToRoomAndStatus("default");
                easyrtc.setAcceptChecker(acceptCheckP2P);
                hb.onclick = function() { hangup(); };
                disable("hangupButton");
                easyrtc.getRoomList(convertRoomsToList, null); 
            }

            var joinFailure = function(){
                easyrtc.joinRoom("default",null,joinDefaultSuccess,null);
                easyrtc.setAcceptChecker(acceptCheckP2P);
                easyrtc.sendPeerMessage(who, "cancelInvitation", "Problema en la aplicación. Intente de nuevo.");
            }

            var leaveRoomSuccess = function(){
                easyrtc.joinRoom(roomName,null,joinRoomSuccess,joinFailure);
            }

            var leaveRoomFailure = function(){
                popupAlert("Fallo al desconectar de ".concat(connectedToRoom));
                easyrtc.sendPeerMessage(who, "cancelInvitation", "Problema en la aplicación. Intente de nuevo.");
            }
            /***********Fin Callbacks***********/
            /***********************************/
        
        //Detengo sonido de llamada entrante
        stopIncommingCallSound();
        enable('hangupButton');

        if (connectedToRoom == "default") 
        {
            easyrtc.joinRoom(roomName,null,joinRoomSuccess,joinFailure);
            updateConnectedToRoomAndStatus(roomName); 
        } else
        {
            easyrtc.hangupAll();
            easyrtc.leaveRoom(connectedToRoom,leaveRoomSuccess,leaveRoomFailure);
        };

         //Oculto el cuadro de dialogo para aceptar y rechazar.
        document.getElementById('acceptCallBox').style.display = "none";
    };
    
    //Usuario oprime rechazar
    document.getElementById("callRejectButton").onclick =function() {
        document.getElementById('acceptCallBox').style.display = "none";
        easyrtc.sendPeerMessage(who, "cancelInvitation", "Invitacion Cancelada");
    };  
}

/* //No hacemos nada, se conectará a la sala, y verá eso.
function acceptedInvitation(who, msgType, content)
{
    //popupAlert("El usuario ");
}
*/

function cancelledInvitation(who, msgType, content)
{
    popupAlert("El usuario "+easyrtc.idToName(who)+" no se conecto a la reunión. "+ content)
}


///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
*
* @class Funciones de file transefering
* 
*/


/**
* Me parece que blob=Binary Large Objects. O sea, el binario.
*
* @method blobAcceptor
* @param otherGuy {string}
* @param blob
* @param filename
*/
function blobAcceptor(otherGuy, blob, filename) {
    //easyrtc_ft.saveAs guarda el archivo en el directorio por defecto del explorador
    easyrtc_ft.saveAs(blob, filename);
}

/**
* Funcion que se llama cuando llega un pedido de envio de archivo al usuario.
*
* @method acceptRejectCB
* @param otherGuy {string}
* @param fileNameList
* @param wasAccepted
*/
function acceptRejectCB(otherGuy, fileNameList, wasAccepted) {

    // Abrir la ventana del chat.
    makeChatWindow(otherGuy);

    //En content podremos lo que enviaremos de respuesta como chat.
    var content = "Aceptar transferencia de arvhivo";

    // Cantidad de archivos que se nos ofrecen
    var filesNum = fileNameList.length;

    // Armar content. Depende de la cantidad de archivos que nos quieren enviar.
    switch(filesNum){
        case 0:
            break;
        case 1:
            content += " "+fileNameList[0].name + " ("+Math.round(fileNameList[0].size * 0.001)+" kB)?";
            break;
        default:
            content+="s";
            for(var i=0;i<filesNum-2;i++){
                content += " "+fileNameList[i].name + " ("+Math.round(fileNameList[i].size * 0.001)+" kB),";
            }
            content += " "+fileNameList[filesNum-2].name + " ("+Math.round(fileNameList[filesNum-2].size * 0.001)+" kB) y";
            content += " "+fileNameList[filesNum-1].name + " ("+Math.round(fileNameList[filesNum-1].size * 0.001)+" kB)?";
            break;
    }

    // Ventana de chat donde vamos a escribir.
    var receiveBlock = document.getElementById("chatWindowText"+otherGuy);

    // Escribimos en la ventana de chat que nos ofrecen los archivos.
    var who = "<b>"+easyrtc.idToName(otherGuy)+"</b>";
    receiveBlock.innerHTML += who + ":&nbsp;" + content + "<br />";

    // Agregamos botonos para aceptar y rechazar las transferencias
    var myID = idButtons;

    var acceptButton = document.createElement("button");
    acceptButton.className = "tawk_button";
    acceptButton.id = "acceptButton"+myID;
    acceptButton.onclick = function(e){
        wasAccepted(true);

        var chatText = document.getElementById("chatText"+otherGuy); 
        var chatAux = chatText.value;
        chatText.value = "Transferencia aceptada."
        sendChatText(otherGuy);
        chatText.value = chatAux;
        chatText.focus();
        
        document.getElementById("rejectButton"+myID).className="tawk_button_disabled";
        disable("rejectButton"+myID);

        document.getElementById("acceptButton"+myID).className="tawk_button_disabled";
        disable("acceptButton"+myID);
        e.preventDefault();
    };
    acceptButton.innerHTML = "Aceptar";
    receiveBlock.appendChild(acceptButton);

    var rejectButton = document.createElement("button");
    rejectButton.className = "tawk_button";
    rejectButton.id = "rejectButton"+idButtons;
    rejectButton.onclick = function(){
        wasAccepted(false);

        var chatText = document.getElementById("chatText"+otherGuy); 
        var chatAux = chatText.value;
        chatText.value = "Transferencia rechazada."
        sendChatText(otherGuy);
        chatText.value = chatAux;
        chatText.focus();

        document.getElementById("rejectButton"+myID).className="tawk_button_disabled";
        disable("rejectButton"+myID);

        document.getElementById("acceptButton"+myID).className="tawk_button_disabled";
        disable("acceptButton"+myID);
    };

    rejectButton.innerHTML = "Rechazar";
    receiveBlock.appendChild(rejectButton);

    var br = document.createElement("br");
    receiveBlock.appendChild(br);

    idButtons += 1;

    receiveBlock.scrollTop = receiveBlock.scrollHeight;
} 

/**
* Funcion que se llaman cuando se actualiza el estado de los archivos que se estan recibiendo. <br> 
* Por ahora, nada. El usuario verá desde el explorador de internet que use como van las cosas.
*
* @method receiveStatusCB
* @param otherGuy {string}
* @param msg {string}
*/
function receiveStatusCB(otherGuy, msg) {

    // var receiveBlock = document.getElementById(buildReceiveAreaName(otherGuy));
    switch (msg.status) {
        case "started":
            //addToConversation(otherGuy,"Started tx", "started tx");
            break;
        case "eof":
            //addToConversation(otherGuy,"Fin de Tx","Ha recibido exitosamente el archivo "+msg.name+".");
            //receiveBlock.innerHTML = "Finished file";
            break;
        case "done":
            //addToConversation(otherGuy,"Done tx", "done tx");
            //receiveBlock.innerHTML = "Stopped because " + msg.reason;
            //setTimeout(function() {
            //    receiveBlock.style.display = "none";
            //}, 1000);
            break;
        case "started_file":
            //addToConversation(otherGuy,"Started_file tx", "started_file tx");
            //receiveBlock.innerHTML = "Beginning receive of " + msg.name;
            break;
        case "progress":
            //addToConversation(otherGuy,"progress tx", "progress tx");
            //receiveBlock.innerHTML = msg.name + " " + msg.received + "/" + msg.size;
            break;
        default:
            console.log("strange file receive cb message = ", JSON.stringify(msg));
    }
    return true;
}
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////



var activeBox = -1;  // nothing selected
var aspectRatio = 4/3;  // standard definition video aspect ratio
var maxCALLERS = 3;
var numVideoOBJS = maxCALLERS+1;
var layout;


easyrtc.dontAddCloseButtons(true);

function getIdOfBox(boxNum) {
    return "box" + boxNum;
}


function reshapeFull(parentw, parenth) {
    /*return {
        left:0,
        top:0,
        width:parentw,
        height:parenth
    };*/
    if (easyrtc.getConnectionCount() == 0) {
        return {
        left:0,
        top:0,
        width:150,
        height:150
        };
    } else{
        return {
        left:0,
        top:0,
        width:parentw,
        height:parenth
        };
    };
}



var margin = 20;

function reshapeToFullSize(parentw, parenth) {
    var left, top, width, height;
    var margin= 20;

    if( parentw < parenth*aspectRatio){
        width = parentw -margin;
        height = width/aspectRatio;
    }
    else {
        height = parenth-margin;
        width = height*aspectRatio;
    }
    left = (parentw - width)/2;
    top = (parenth - height)/2;
    /*return {
        left:left,
        top:top,
        width:width,
        height:height
    };*/
    if (easyrtc.getConnectionCount() == 0) {
        return {
        left:left,
        top:top,
        width:150,
        height:150
        };
    } else{
        return {
        left:left,
        top:top,
        width:width,
        height:height
        };
    };
}

//
// a negative percentLeft is interpreted as setting the right edge of the object
// that distance from the right edge of the parent.
// Similar for percentTop.
//
function setThumbSizeAspect(percentSize, percentLeft, percentTop, parentw, parenth, aspect) {

    var width, height;
    if( parentw < parenth*aspectRatio){
        width = parentw * percentSize;
        height = width/aspect;
    }
    else {
        height = parenth * percentSize;
        width = height*aspect;
    }
    var left;
    if( percentLeft < 0) {
        left = parentw - width;
    }
    else {
        left = 0;
    }
    left += Math.floor(percentLeft*parentw);
    var top = 0;
    if( percentTop < 0) {
        top = parenth - height;
    }
    else {
        top = 0;
    }
    top += Math.floor(percentTop*parenth);
    return {
        left:left,
        top:top,
        width:width,
        height:height
    };
}


function setThumbSize(percentSize, percentLeft, percentTop, parentw, parenth) {
    return setThumbSizeAspect(percentSize, percentLeft, percentTop, parentw, parenth, aspectRatio);
}

function setThumbSizeButton(percentSize, percentLeft, percentTop, parentw, parenth, imagew, imageh) {
    return setThumbSizeAspect(percentSize, percentLeft, percentTop, parentw, parenth, imagew/imageh);
}


var sharedVideoWidth  = 1;
var sharedVideoHeight = 1;

function reshape1of2(parentw, parenth) {
    if( layout== 'p' ) {
        return {
            left: (parentw-sharedVideoWidth)/2,
            top:  (parenth -sharedVideoHeight*2)/3,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        };
    }
    else {
        return{
            left: (parentw-sharedVideoWidth*2)/3,
            top:  (parenth -sharedVideoHeight)/2,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        }
    }
}



function reshape2of2(parentw, parenth){
    if( layout== 'p' ) {
        return {
            left: (parentw-sharedVideoWidth)/2,
            top:  (parenth -sharedVideoHeight*2)/3 *2 + sharedVideoHeight,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        };
    }
    else {
        return{
            left: (parentw-sharedVideoWidth*2)/3 *2 + sharedVideoWidth,
            top:  (parenth -sharedVideoHeight)/2,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        }
    }
}

function reshape1of3(parentw, parenth) {
    if( layout== 'p' ) {
        return {
            left: (parentw-sharedVideoWidth)/2,
            top:  (parenth -sharedVideoHeight*3)/4 ,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        };
    }
    else {
        return{
            left: (parentw-sharedVideoWidth*2)/3,
            top:  (parenth -sharedVideoHeight*2)/3,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        }
    }
}

function reshape2of3(parentw, parenth){
    if( layout== 'p' ) {
        return {
            left: (parentw-sharedVideoWidth)/2,
            top:  (parenth -sharedVideoHeight*3)/4*2+ sharedVideoHeight,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        };
    }
    else {
        return{
            left: (parentw-sharedVideoWidth*2)/3*2+sharedVideoWidth,
            top:  (parenth -sharedVideoHeight*2)/3,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        }
    }
}

function reshape3of3(parentw, parenth) {
    if( layout== 'p' ) {
        return {
            left: (parentw-sharedVideoWidth)/2,
            top:  (parenth -sharedVideoHeight*3)/4*3+ sharedVideoHeight*2,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        };
    }
    else {
        return{
            left: (parentw-sharedVideoWidth*2)/3*1.5+sharedVideoWidth/2,
            top:  (parenth -sharedVideoHeight*2)/3*2+ sharedVideoHeight,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        }
    }
}


function reshape1of4(parentw, parenth) {
    return {
        left: (parentw - sharedVideoWidth*2)/3,
        top: (parenth - sharedVideoHeight*2)/3,
        width: sharedVideoWidth,
        height: sharedVideoHeight
    }
}

function reshape2of4(parentw, parenth) {
    return {
        left: (parentw - sharedVideoWidth*2)/3*2+ sharedVideoWidth,
        top: (parenth - sharedVideoHeight*2)/3,
        width: sharedVideoWidth,
        height: sharedVideoHeight
    }
}
function reshape3of4(parentw, parenth) {
    return {
        left: (parentw - sharedVideoWidth*2)/3,
        top: (parenth - sharedVideoHeight*2)/3*2 + sharedVideoHeight,
        width: sharedVideoWidth,
        height: sharedVideoHeight
    }
}

function reshape4of4(parentw, parenth) {
    return {
        left: (parentw - sharedVideoWidth*2)/3*2 + sharedVideoWidth,
        top: (parenth - sharedVideoHeight*2)/3*2 + sharedVideoHeight,
        width: sharedVideoWidth,
        height: sharedVideoHeight
    }
}

var boxUsed = [true, false, false, false];
var connectCount = 0;


function setSharedVideoSize(parentw, parenth) {
    layout = ((parentw /aspectRatio) < parenth)?'p':'l';
    var w, h;

    function sizeBy(fullsize, numVideos) {
        return (fullsize - margin*(numVideos+1) )/numVideos;
    }

    switch(layout+(connectCount+1)) {
        case 'p1':
        case 'l1':
            w = sizeBy(parentw, 1);
            h = sizeBy(parenth, 1);
            break;
        case 'l2':
            w = sizeBy(parentw, 2);
            h = sizeBy(parenth, 1);
            break;
        case 'p2':
            w = sizeBy(parentw, 1);
            h = sizeBy(parenth, 2);
            break;
        case 'p4':
        case 'l4':
        case 'l3':
            w = sizeBy(parentw, 2);
            h = sizeBy(parenth, 2);
            break;
        case 'p3':
            w = sizeBy(parentw, 1);
            h = sizeBy(parenth, 3);
            break;
    }
    sharedVideoWidth = Math.min(w, h * aspectRatio);
    sharedVideoHeight = Math.min(h, w/aspectRatio);
}

var reshapeThumbs = [
    function(parentw, parenth) {

        if( activeBox > 0 ) {
            return setThumbSize(0.20, 0.01, 0.01, parentw, parenth);
        }
        else {
            setSharedVideoSize(parentw, parenth)
            switch(connectCount) {
                case 0:return reshapeToFullSize(parentw, parenth);
                case 1:return reshape1of2(parentw, parenth);
                case 2:return reshape1of3(parentw, parenth);
                case 3:return reshape1of4(parentw, parenth);
            }
        }
    },
    function(parentw, parenth) {
        if( activeBox >= 0 || !boxUsed[1]) {
            return setThumbSize(0.20, 0.01, -0.01, parentw, parenth);
        }
        else{
            switch(connectCount) {
                case 1:
                    return reshape2of2(parentw, parenth);
                case 2:
                    return reshape2of3(parentw, parenth);
                case 3:
                    return reshape2of4(parentw, parenth);
            }
        }
    },
    function(parentw, parenth) {
        if( activeBox >= 0 || !boxUsed[2] ) {
            return setThumbSize(0.20, -0.01, 0.01, parentw, parenth);
        }
        else  {
            switch(connectCount){
                case 1:
                    return reshape2of2(parentw, parenth);
                case 2:
                    if( !boxUsed[1]) {
                        return reshape2of3(parentw, parenth);
                    }
                    else {
                        return reshape3of3(parentw, parenth);
                    }
                case 3:
                    return reshape3of4(parentw, parenth);
            }
        }
    },
    function(parentw, parenth) {
        if( activeBox >= 0 || !boxUsed[3]) {
            return setThumbSize(0.20, -0.01, -0.01, parentw, parenth);
        }
        else{
            switch(connectCount){
                case 1:
                    return reshape2of2(parentw, parenth);
                case 2:
                    return reshape3of3(parentw, parenth);
                case 3:
                    return reshape4of4(parentw, parenth);
            }
        }
    },
];


function setReshaper(elementId, reshapeFn) {
    var element = document.getElementById(elementId);
    if( !element) {
        alert("Attempt to apply to reshapeFn to non-existent element " + elementId);
    }
    if( !reshapeFn) {
        alert("Attempt to apply misnamed reshapeFn to element " + elementId);
    }
    element.reshapeMe = reshapeFn;
}


function collapseToThumbHelper() {
    if( activeBox >= 0) {
        var id = getIdOfBox(activeBox);
        document.getElementById(id).style.zIndex = 2;
        setReshaper(id, reshapeThumbs[activeBox]);
        activeBox = -1;
    }
}

function collapseToThumb() {
    collapseToThumbHelper();
    activeBox = -1;
    handleWindowResize();

}


function expandThumb(whichBox) {
    var lastActiveBox = activeBox;
    if( activeBox >= 0 ) {
        collapseToThumbHelper();
    }
    if( lastActiveBox != whichBox) {
        var id = getIdOfBox(whichBox);
        activeBox = whichBox;
        setReshaper(id, reshapeToFullSize);
        document.getElementById(id).style.zIndex = 1;
    }
    handleWindowResize();
}

function prepVideoBox(whichBox) {
    var id = getIdOfBox(whichBox);
    setReshaper(id, reshapeThumbs[whichBox]);
    document.getElementById(id).onclick = function() {
        expandThumb(whichBox);
    };
}

function killActiveBox() {
    if( activeBox > 0) {
        var easyrtcid = easyrtc.getIthCaller(activeBox-1);
        collapseToThumb();
        setTimeout( function() {
            easyrtc.hangup(easyrtcid);
        }, 400);
    }
}

function handleWindowResize() {
    document.getElementById('containerVideos').style.width = $("#main").width()-240 + "px";
    //document.getElementById('videos').style.width = $("#main").width()-240 + "px";
    var fullpage = document.getElementById('videos');
    fullpage.style.height = $("#containerVideos").height() + "px";
    fullpage.style.width = $("#containerVideos").width() + "px";
    connectCount = easyrtc.getConnectionCount();

    function applyReshape(obj,  parentw, parenth) {
        var myReshape = obj.reshapeMe(parentw, parenth);

        if(typeof myReshape.left !== 'undefined' ) {
            obj.style.left = Math.round(myReshape.left) + "px";
        }
        if(typeof myReshape.top !== 'undefined' ) {
            obj.style.top = Math.round(myReshape.top) + "px";
        }
        if(typeof myReshape.width !== 'undefined' ) {
            obj.style.width = Math.round(myReshape.width) + "px";
        }
        if(typeof myReshape.height !== 'undefined' ) {
            obj.style.height = Math.round(myReshape.height) + "px";
        }

        var n = obj.childNodes.length;
        for(var i = 0; i < n; i++ ) {
            var childNode = obj.childNodes[i];
            if( childNode.reshapeMe) {
                applyReshape(childNode, myReshape.width, myReshape.height);
            }
        }
    }

    applyReshape(fullpage, $("#containerVideos").width(), $("#containerVideos").height());
}
