/**
 * Implementacion de funciones de cookies.
 * @module cookies.js
 */
 
/**
* Funciones setCookie y getCookie.
* @class Funciones de seteo y recuperación de valor de cookies.
*/

 
/**
* Función que establece una cookie
*
* @method setCookie
* @param cname {string} Nombre de la cookie a setear.
* @param cvalue {string} Valor que le daremos a la cookie.
* @param exdays {int} Cantidad de días que tendrá la cookie como válida.
*/

function setCookie(cname,cvalue,exdays)
{
  var d = new Date();
  d.setTime(d.getTime()+(exdays*24*60*60*1000));
  var expires = "expires="+d.toGMTString();
  document.cookie = cname + "=" + cvalue + "; " + expires;
}

/**
* Función que devuelve el valor de una cookie
*
* @method getCookie
* @param cname {string} Nombre de la cookie de la cual se quiere encontrar el valor o "" si no existe la cookie.
* @example 
*        //En el connect de conferenciasSala.js
*        //Obtenemos nombre de la sala
*        roomName = getCookie("roomName");
*/
function getCookie(cname)
{
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) 
  {
    var c = ca[i].trim();
    if (c.indexOf(name)==0) 
      return c.substring(name.length,c.length);
  }
  return "";
}

function checkCookie()
{
  var user=getCookie("username");
  if (user!="")
  {
    alert("Welcome again " + user);
  }
  else 
  {
    user = prompt("Please enter your name:","");
    if (user!="" && user!=null)
    {
      setCookie("username",user,365);
    }
  }
}
