/**
 * Implementacion de la logica de la aplicacion hecha por nosotros. <br>
 * Utilizamos Modulo como un archivo. <br> Y a lo que le llama clases, 
 * solamente son funciones que las agrupamos por la funcionalidad, pero no pertenecen a una clase per se.
 *
 * @module ConferenciasSala.js
 */



/**
* Aqui en la documentacion no listamos todas las que hay, solo las mas importantes.
*
* @class Variables Globales Sala
* 
*/
sglm[0]='';

var zindex = 0;             //variable global, para que caundo hacemos click en una ventana se aumente su z-index y pase al frente.
var selfEasyrtcid = "";     //Mi id.
var uname="";               //Mi nombre de usuario.
var umail="";               //Mi nombre de usuario de mail.
var smail="";               //Mi dominio de usuario de mail.
/**
* Array con los id's de las ventanas donde se posicionaran 
* 
* @property videoIds
* @type {[string]}
*/
//var videoIds = ["callerVideo1", "callerVideo2", "callerVideo3", "callerVideo4", "callerVideo5"];
var videoIds = ["box1", "box2", "box3"];
/**
* Tenemos dos opciones de estar en las salas. Estar solo en la default (D), ó,  en la default y además otra sala(S). 
* Eso significa que nunca dejamos la sala default. <br>
* Si se da la primera situación , connectedToRoom = D. En la segunda situación, connectedToRoom = S. <br>
* Podemos utilizar esta variable para chequear la sala en la que está y además si esta conectado a una sola sala, o a dos.
* 
* @property connectedToRoom
* @type {string}
* @default "default"
*/
var connectedToRoom;
/**
* Lista de salas creadas.
* 
* @property currentRoomList
* @type {Object}
*/   
var currentRoomList;
var talkingTo = null;
var cancelCall;
var incommingCallSound;
/**
* Estado del usuario. away(ausente),chat(no disponible),dnd(),xa(disponible)
* 
* @property currentShowState
* @type {string}
* @default "xa"
*/ 
var currentShowState = "xa"; //away(ausente),chat(no disponible),dnd(),xa(disponible)
/**
* Textos de estados del usuario. Disponible, No Disponible, "" .
* 
* @property currentShowText
* @type {Object}
* @default ""
*/ 
var currentShowText = "";
var roomUsers;
var idButtons = 0;

//Elementos de fondo.
var backgroundsFolder = "images/backgrounds/"
var backgrounds = ["b3.jpg","b1.jpg","b2.jpg","background.jpg"];
var currentBackground = 0;

//easyrtc.onError = function(errorObject) { console.log("Entre a onerror conferenciasSala"); };
easyrtc.onError = function(errorObject) { 
    console.log("Entre a onerror conferenciasSala, con error: "+errorObject.errorText);
    popupAlert(errorObject.errorText);
    };


/**
* Cambia las imagenes de fondo. Se mueve circularmente por todas las imágenes.
* @method changeBackground
*/
function changeBackground() {
    currentBackground += 1;
    currentBackground %= backgrounds.length;
    //document.body.style.backgroundImage="url("+backgroundsFolder+backgrounds[currentBackground]+")";
    $('body').css("background-image", "url("+backgroundsFolder+backgrounds[currentBackground]+")");
}

//Oculta elemento por id.
function oculta(id) {
    //document.getElementById(id).visibility = "hidden";
    //document.getElementById(id).style.display = "none";
    $('#'+id).hide();
}

function muestra(id) {
    //document.getElementById(id).visibility = "visible";
    //document.getElementById(id).style.display = "block";
    $('#'+id).show();
}

//Desabilita elemento por id. Aparece de colore gris.
function disable(id) {
    //console.log("about to try disabling "  +id);
    document.getElementById(id).disabled = "disabled";
}

//Habilita elemento por id.
function enable(id) {
    //console.log("about to try enabling "  +id);
    document.getElementById(id).disabled = "";
}

///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
* Connect, callbacks de actualizaciones cuando hay cambios en las salas.
* @class Funciones de conexion y desconexion al servidor Sala
* 
*/


//Conecta a la sala default del servidor.
$(document).ready(function(){
    roomName = getCookie("roomName");
    $("#roomNameLabel").html("Bienvenidos a la runión: "+roomName);
});

/**
* Conexion al principio, cuando se entra en la página (acceso al servicio).
* Se setean algunos callbacks necesarios, como por ejemplo el que escucha los cambios de usuarios en las salas a las que pertenecemos. <br>
* A diferencia del connect() de conferencias.js, hacemos joinRoom antes de hacer easyrtc.easyApp, asi solo nos conectamos a la sala deseada y no a al default también.
* @method connect
*/
function connect() {
    //Habilitamos debugueo en consola.
    //easyrtc.enableDebug(true);

   
    //Obtenemos nombre de la sala
    roomName = getCookie("roomName");
    uname = document.getElementById("userName").value;
    //Si es una sala sin password, no seteamos las credenciales, así cuando se lanza "autehnticate", en el server pasa derecho sin comprobar el pass.
    if (document.getElementById("typeOfRoom").value === "passwordless")
    {
        //pass = "";
    }
    else
    {
        pass = document.getElementById("password").value;
         //usadas en el server, en onAuthenticate
         easyrtc.setCredential({ 
            "roomName": roomName,
            "userName" : uname,  
            "password" : pass, 
        });
    }
    connectedToRoom = roomName;

    //Nombre de usuario debe tener caracteres y caracteristicas validas.
    if ( !easyrtc.setUsername(uname) ) {
        $('#userName').attr("placeholder", "Nombre de usuario inválido");
        document.getElementById("userName").focus();
        return false;
    }
    
    /*var vidResol = document.getElementById('resolution').value;
    switch(vidResol){
        case "Baja":
            easyrtc.setVideoDims(320,240);
        case "Alta":
            easyrtc.setVideoDims();
        default:
            easyrtc.setVideoDims(640,480);
    }*/



    easyrtc.setPeerListener(addToConversation);
    //easyrtc.enableDataChannels(true);

    //Nos conectamos a la sala que tenemos guardada en la cookie, prevenimos de conectarnos a la default.
    easyrtc.joinRoom(roomName, null, null, null);

    //Inicializacion de EasyRTC. se llama al evento "authenticate".
    easyrtc.easyApp(
        "Transatlantica", 
        "box0", 
        videoIds, 
        loginSuccess, 
        loginFailure);

    //Funcion a la que se llama cuando cambia la lista de usuarios conectados.
    //Se registra el callback para averiguar quien mas (Ademas de self) esta conectado al server.
    easyrtc.setRoomOccupantListener(convertListToButtons);

    //Codigo para el aceptador de la llamada
    easyrtc.setAcceptChecker(acceptCheckMultiparty);
    //easyrtc.setAcceptChecker(acceptCheck);

    easyrtc.setOnCall( function(easyrtcid, slot) {
        boxUsed[slot+1] = true;
        if(activeBox == 0 &&  easyrtc.getConnectionCount() == 1) { // first connection
            collapseToThumb();
        }
        document.getElementById(getIdOfBox(slot+1)).style.visibility = "visible";
        handleWindowResize();
    });


    easyrtc.setOnHangup(function(easyrtcid, slot) {
        boxUsed[slot+1] = false;
        console.log("hanging up on " + easyrtcid);
        if(activeBox > 0 && slot+1 == activeBox) {
            collapseToThumb();
        }
        setTimeout(function() {
            document.getElementById(getIdOfBox(slot+1)).style.visibility = "hidden";

            if( easyrtc.getConnectionCount() == 0 ) { // no more connections
                expandThumb(0);
            }
            handleWindowResize();
        },20);
        updatePresence();
        console.log("call with " + easyrtcid + "ended");
    });

    //Seteamos esto, asi cuando se conecta, se le ordenan bien los contactos.
    //updateConnectedToRoomAndStatus(roomName);
}

/**
* Funcion a la que se llama si es correcto el logueo de entrada en la sala. 
* Si es correcto, mostramos todos los elementos, ventans de chat, botones, etc.
*
* @method loginSuccess
* @param easyrtcId {easyrtcid} Seteamos este id como el nuestro. <br> selfEasyrtcid = easyrtcId; 
*/
function loginSuccess(easyrtcId) {
    selfEasyrtcid = easyrtcId;

    //Reordenamos la pantalla
    muestra("rightWrapperTopBar");
    muestra("userNameBlock");
    enable('otherClients');
    muestra('otherClients');
    muestra('labelConnectedUsers');
    oculta('loginBox');
    muestra('connectControls');
    document.getElementById("box0").style.visibility = "visible";

    // Prep for the top-down layout manager
    setReshaper('videos', reshapeFull);
    for(var i = 0; i < numVideoOBJS; i++) {
        prepVideoBox(i);
    }
    window.onresize = handleWindowResize;
    handleWindowResize(); //initial call of the top-down layout manager
    expandThumb(0);

    startbcscroll();//Arrancamos el scroller de mensajes en el topbar

    //Cambiamos la forma en la que mostramos los errores
    //Si en algún lado que sabemos que da error, mostramos un mensaje con popupAlert, 
    //Este mensaje no se mostrará. Ya que le ventana donde se muestran los mensajes está siendo usada ya.
     easyrtc.setOnError( function(errorObject){
       console.log("setOnError conferenciasSala con error: " +errorObject.errorText);
       popupAlert(errorObject.errorText);
    });

    easyrtc.setDisconnectListener(function(){
       easyrtc.showError("SYSTEM-ERROR", "Se perdió la conexión al socket server");
    });

    updateConnectedToRoomAndStatus(connectedToRoom);
    
    //Obtenemos la lista de salas convertRoomsToList, llama a la funcion argumentada con dicha data.
    //easyrtc.getRoomList(convertRoomsToList, errorList);


}

//Funcion a la que se llama si es fallido el logueo de easyApp
function loginFailure(errorCode, message) {
    //Si no recargo, me dice que ya esta conectado al socketserver y no entra.
    //var reloadPage = setTimeout(function(){document.location.reload(false)},2000);
    //popupAlert("Password Incorrecto, intente de nuevo.");

    if ($('#password').length > 0)
        $('#password').attr("placeholder", "Password inválido");
    else
        popupAlert("Password Incorrecto, intente de nuevo.");
    
    easyrtc.disconnect();
    console.log("Failed to connect. errorCode: "+errorCode+"\n message: "+message);
}

function errorList(errorCode, message) {
    console.log("Failed to list rooms. errorCode: "+errorCode+"\n message: "+message);
}


//Desconectarse del servidor.
function disconnect() {
    window.location.reload(false);
}




///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
* Funciones para llamarse entre usuarios, y colgar llamadas.
*
* @class Funciones de Llamada y Desconexion con Usuarios Sala
* 
*/

/**
* Llamar a un usuario cortando previamente otras conexiones (para P2P).
* @method performCall
* @param otherEasyrtcid {easyrtcid} Id del usuario al que vamos a llamar.
*/
function performCall(otherEasyrtcid) {
    //Corto las llamadas actuales.
    easyrtc.hangupAll();
    talkingTo = null;

    //Armo y muestro el div de llamada saliente.
    document.getElementById("callingToLabel").innerHTML="Llamando a "+easyrtc.idToName(otherEasyrtcid);
    cancelCall = setTimeout(function(){document.getElementById("cancelCall").click()},30000);
    muestra("callingToBox");

    //updatePresence();
    updateConnectedToRoomAndStatus(connectedToRoom);
    performCallAux(otherEasyrtcid);
}

//Llamar a un usuario (argumento).
function performCallAux(otherEasyrtcid) {

    //Funcion que toma la respuesta.
    var acceptedCB = function(accepted, caller) {
        //Si la llamada fue p2p, cancelar el timeout y ocultar el box de llamada saliente.
        clearTimeout(cancelCall);
        oculta("callingToBox");

        //Llamada rechazada.
        if( !accepted ) {
            enable('otherClients');
        }
        //Llamada aceptada
        else{
            if(connectedToRoom == "default"){
                talkingTo = otherEasyrtcid;
                document.getElementById(talkingTo).className="connected";
                //Ponemos al llamante 1 en div principal.
            }
        }
    };

    //Realización de llamada exitosa.
    var successCB = function() {
        console.log("Llamando a "+easyrtc.idToName(otherEasyrtcid));
    };

    //Realización de llamada errónea.
    var failureCB = function() {
        enable('otherClients');
    };

    //easyrtc.enableDataChannels(true);
    easyrtc.call(otherEasyrtcid, successCB, failureCB, acceptedCB);
}

/**
* Colgar todas las llamadas.
* @method hangup
*/
function hangup() {
    //Ocultamos la caja de llamada saliente
    oculta("callingToBox");

    //Cortamos todas las llamadas actuales.
    easyrtc.hangupAll();
    talkingTo=null;

    updatePresence();
    disable('hangupButton');
} ///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
* Funciones que manejan como se ven reflejado, los cambios en las salas, en el usuario. <br>
* Son callbacks seteados en connect.
* @class Funciones de Sala
*/

/**
* Función seteaa como callback. <br>
* Funcion a la que se llama cada vez que hay algun cambio en la lista de usuarios logueados en alguna de las salas (a la que pertenezcamos). En data viene los datos de la sala que fue modificada. <br>
* En  roomUsers vamos a tener guardada la data de la ultima configuracion de usuarios en la reunion <br>
*
* @method convertListToButtons
* @param roomName {string} Nombre de la sala, a la que pertenecemos, que se está acutalizando.
* @param data {obj} Datos de los otros usuarios, que no soy yo, que están en la sala roomName
* @param isPrimary
*/
function convertListToButtons (roomName, data, isPrimary) {

    if (roomName === null) {
        return;
    }


    roomUsers = data;

    //Borramos todos los elementos en otherClients
    clearConnectList();

    var otherClientDiv = document.getElementById("otherClients");
    
    var listSize = 0;
    otherClientDiv = document.getElementById('otherClients');
    //creamos un titulo con el nombre de la sala
    var meetingName = document.createElement('h4');
    meetingName.className = "nombreSalas";
    meetingName.innerHTML = "Usuarios en: " + connectedToRoom;
    meetingName.innerHTML +="<img src='images/chat.png' style='float:right;' class='links' onclick='makeChatWindow(connectedToRoom);'>";
    otherClientDiv.appendChild(meetingName);
    //ponemos los usuarios de la sala.
    var listElement = document.createElement('ul');
    listElement.className="users";
    otherClientDiv.appendChild(listElement);

    for(var i in roomUsers) {
        var listItem = document.createElement('li');
        listItem.innerHTML = easyrtc.idToName(i).substring(0,23)+(easyrtc.idToName(i).length > 16 ? "..." : "");
        listElement.appendChild(listItem);
        listSize = listSize + 1;
        if(easyrtc.getSlotOfCaller(i) == -1 && i < selfEasyrtcid) {
            performCallAux(i);
        }
    }//for
}//function



///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
*
* @class Funciones de callback  para cuando hay llamadas entrantes Sala
* 
*/

//Funcion aceptadora de llamadas en modo multiparty 
function acceptCheckMultiparty(caller, helper) {
    //Ponemos a quien caiga en el primer video en el div principal

    //Buscamos un lugar para la nueva llamada
    for (var i = 0; i < maxCALLERS; i++) {
        var video = getIthVideo(i);
        if (videoIsFree(video)) {
            helper(true);
            return;
        }
    }

    //No había lugar para la nueva llamada.
    helper(false);
}



///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
*
* @class Funciones de configuración de estado y texto personalizado de usuario Sala
* 
*/

/**
* Actualiza el ESTADO del usuario.
* @method setPresence
* @param value {string} Puede ser uno de los tres valores : "Disponible", "No disponible", "Ausente"
*/
function setPresence(value) {
    //away(ausente),chat(no disponible),dnd(),xa(disponible)
    switch(value){
        case "Disponible":
            currentShowState = "xa";
            break;
        case "No disponible":
            currentShowState = "chat";
            break;
        default:
            currentShowState = "away";
            break
    }
    updatePresence();
}

/**
* Actualiza el TEXTO del usuario.
* @method updatePresenceStatus
* @param value {string} 
*/
function updatePresenceStatus(value) {
    currentShowText = value;
    updatePresence();
}

/**
* Actualiza el estado y el texto del usuario. Llama a convertListToButtons
* @method updatePresence
*/
function updatePresence()
{
    easyrtc.updatePresence(currentShowState, currentShowText);
}

/**
* Funcion que actualiza la sala a la cual está, y también el estado, para que cada vez que 
* se haga algún cambio, llame a convertListToButtons, y se actualize de manera correcta las listas de usuarios en cada sala.
*
* @method updateConnectedToRoomAndStatus
* @param room {string} Nombre de la sala a la cual estamos conectados. Seguimos la regla definida en la propiedad connectedToRoom.
*/
function updateConnectedToRoomAndStatus(room)
{
    connectedToRoom = room;
    //Me actualizo yo, y se le actualiza a los demás usuarios.
    updatePresenceStatus(room);

    convertListToButtons(connectedToRoom, roomUsers, false);
}

///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
*
* @class Funciones auxiliares Sala
* 
*/

/**
* Muestra un mensaje estilo popup.
* @method popupAlert
* @param mensaje {string}
*/
function popupAlert(mensaje){
    document.getElementById('popupMessage').style.display = "block";
    document.getElementById('textToShowInPopup').innerHTML = mensaje;

    //Usuario oprime Aceptar
    document.getElementById("popupBoxButton").onclick = function() {
        document.getElementById('popupMessage').style.display = "none";
    };
}

//Para debuguear y ver los usuarios en la consola.
function imprimirUsuarios(roomName)
{
    if (roomName == "default") 
    {
        for(var i in roomDefault)
            console.log(easyrtc.idToName(i));
    } 
    else
    {
        for(var i in roomUsers)
            console.log(easyrtc.idToName(i));
    }
}

/**
* Elimina toda la informacion dentro de otherClients, o sea de la lista de los otros usuarios que vemos en las salas.
* @method clearConnectList
*/
function clearConnectList() {
    otherClientDiv = document.getElementById('otherClients');
    while (otherClientDiv.hasChildNodes()) {
        otherClientDiv.removeChild(otherClientDiv.lastChild);
    }
}



/**
* Predicado para saber si estamos o no conectados con otro usuario mediante video.
* @method notConnectedTo
* @param id {}
*/
function notConnectedTo(id){
    for (var i = 0; i < maxCALLERS; i++) {
        var video = getIthVideo(i);
        if (!videoIsFree(video)) {
            if(video.caller == id){
                return false;
            }
        }
    }
    return true;
}

/**
* Funcion que retorna el i-esimo video (de haber alguno)
* @method getIthVideo
* @param i {int}
* @return null ó div de video con id i
*/
function getIthVideo(i) {
    if (videoIds[i]) {
        return document.getElementById(videoIds[i]);
    }
    else {
        return null;
    }
}

/**
* Predicado que establece si un objeto de video esta libre.
* @method videoIsFree
* @param obj {obj}
* @return {bool}
*/
function videoIsFree(obj) {
    return (obj.caller === "" || obj.caller === null || obj.caller === undefined);
}

/**
* Funcion encargada de emitir sonido cuando hay una llamada entrante.
* @method playIncommingCallSound
*/
function playIncommingCallSound() {
    document.getElementById('incommingCall').play();
    incommingCallSound = setTimeout(playIncommingCallSound,50);
}

/**
* Funcion que detiene la reproduccion de un sonido.
* @method stopIncommingCallSound
*/
function stopIncommingCallSound() {
    clearTimeout(incommingCallSound);
}

/**
* Nos dice si el parámetro pasado es un easyrtcid válido (fijandonos si esta en alguna sala).
* @method isValidEasyrtcid
* @param who Id que queremos buscar
* @param roomData Sala donde lo queremos buscar
* @return {bool}
*/
function isValidEasyrtcid(who, roomData)
{
    var res = false;
    for(var i in roomData){
        if(i == who)
            res = true;
    }
    return res;
}

function addToScroller(msg)
{
    //solo la 1era vez de todo entra acá. Sino imprime undefined.
    if(sglm[0]=='')
        sglm.shift();

    if(sglm.length < 4)
        sglm.push(msg);
    else{
        sglm.shift(); //sacamos el 1 elemento
        sglm.push(msg); //ponemos el elemento en ultimo lugar   
    }
}

var originalTitle = document.title;
var blinkTitle;
var blinkLogicState = false;

function StartBlinking(title){
    StopBlinking();
    blinkTitle = title; 
    BlinkIteration();
}

function BlinkIteration(){
    if(blinkLogicState == false)
        document.title = blinkTitle;
    else
        document.title = originalTitle;
    
    blinkLogicState = !blinkLogicState;
    blinkHandler = setTimeout(BlinkIteration, 1000);
}
    
function StopBlinking(){
    if(blinkHandler)
        clearTimeout(blinkHandler);
    
    document.title = originalTitle;
}

///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///FUNCIONES AUXILIARES DEL EJEMPLO ORIGINAL. LA MAYORIA SOBREESCRITAS POR easyrtc.easyApp

var callerPending = null;

easyrtc.setStreamAcceptor( function(caller, stream) {
    setUpMirror();
    var video = document.getElementById('callerVideo1');
    easyrtc.setVideoObjectSrc(video,stream);
    console.log("saw video from " + caller);
});

easyrtc.setOnStreamClosed( function (caller) {
    easyrtc.setVideoObjectSrc(document.getElementById('callerVideo1'), "");
});

easyrtc.setCallCancelled( function(caller){
    stopIncommingCallSound();
    if( caller === callerPending) {
        document.getElementById('acceptCallBox').style.display = "none";
        callerPending = false;
    }
});


//????????????????????????????
function setUpMirror() {
    //~ if( !haveSelfVideo) {
        //~ var selfVideo = document.getElementById("selfVideo");
        //~ easyrtc.setVideoObjectSrc(selfVideo, easyrtc.getLocalStream());
        //~ selfVideo.muted = true;
        //~ haveSelfVideo = true;
    //~ }    
}

///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
/**
*
* @class Funciones de chat Sala
* 
*/

/**
* Creamos ventanas de chat. Creamos dos versiones de ventanas, una grande y otra chiquita. Cuando minimizamos o maximizamos, hacemos hide de una y hose de la otra.
*
* @method makeChatWindow
* @param who {string} El nombre de la sala.
*/
//Creamos dos versiones de ventanas, una grande y otra chiquita. Cuando minimizamos o maximizamos, hacemos hide de una y hose de la otra.
function makeChatWindow(who) {

    //Si ya existe, la maximizamos y listo, no la volvemos a crear.
    if (document.getElementById("chatWindowTopLevel"+who)!=null) {
        muestra("chatWindow"+who);
        oculta("chatWindowMin"+who);
        return;
    }

    var chatWindows = document.getElementById("chatWindows");

    //Creamos ventana
    var chatWindowTopLevel = document.createElement("div");
    chatWindowTopLevel.id = "chatWindowTopLevel"+who;
    chatWindowTopLevel.className = "chatWindowTopLevel";
    chatWindowTopLevel.onclick = function () {document.getElementById("chatWindowTopLevel"+who).style.zIndex = zindex+1;};

    var chatWindow = document.createElement("div");
    chatWindow.id="chatWindow"+who;
    chatWindow.className = "chatWindow";
    chatWindow.style.display="block";

    var chatWindowTopBar = document.createElement("div");
    chatWindowTopBar.id = "chatWindowTopBar";
    chatWindowTopBar.className = "chatWindowTopBar";

    //nombre de con quien chateamos
    var chattingWith = document.createElement("label");
    chattingWith.id = "chattingWith"+who;
    chattingWith.innerHTML = easyrtc.idToName(who);
    chattingWith.className = "chattingWith";

    //boton para cerrar la ventana (la ocultamos)
    var closeChatBut = document.createElement("img");
    closeChatBut.src="images/cerrar.png";
    closeChatBut.onclick = function () {closeChat(who);};
    closeChatBut.className= "chatWindowButton";
    closeChatBut.id="closeChat"+who;

    chatWindowTopBar.appendChild(chattingWith);
    chatWindowTopBar.appendChild(closeChatBut);

    //Boton para minimizar la ventana (ocultamos la grande y hacemos aparecer la chiquita)
    var minimizeChat = document.createElement("img");
    minimizeChat.src="images/minimizar.png";
    minimizeChat.onclick = function () {toggleChat(who);};
    minimizeChat.className= "chatWindowButton";
    minimizeChat.id="minimizeChat"+who;
    chatWindowTopBar.appendChild(minimizeChat);

    chatWindow.appendChild(chatWindowTopBar);

    var chatWindowText = document.createElement("div");
    chatWindowText.id = "chatWindowText"+who;
    chatWindowText.className = "chatWindowText";

    chatWindow.appendChild(chatWindowText);

    //donde ingresamos el texto que vamos a enviar
    var chatWindowInput = document.createElement("div");
    chatWindowInput.id = "chatWindowInput";
    chatWindowInput.className = "chatWindowInput";
    chatWindowInput.onclick = function () {StopBlinking();};

    var chatText = document.createElement("input");
    chatText.id = "chatText" + who;
    chatText.onkeydown = function(event){if (event.keyCode == 13) document.getElementById('sendChat'+who).click()};
    chatText.className = "chatText tawk_password_field";

    //boton de enviar texto
    var sendChat = document.createElement("button");
    sendChat.id = "sendChat" + who;
    sendChat.className = "sendChat tawk_button";
    sendChat.innerHTML = "Enviar";
    sendChat.onclick = function() {sendChatText(who);};

    chatWindowInput.appendChild(chatText);
    chatWindowInput.appendChild(sendChat);

    chatWindow.appendChild(chatWindowInput);

    /////////////////////////////////////////////
    ////// Versión minimizada de la ventana /////
    var chatWindowMin = document.createElement("div");
    chatWindowMin.id = "chatWindowMin"+who;
    chatWindowMin.className = "chatWindowMin";

    var chatWindowMinBar = document.createElement("div");
    chatWindowMinBar.id = "chatWindowMinBar"+who;
    chatWindowMinBar.className = "chatWindowMinBar";

    var chattingWithMin = document.createElement("label");
    chattingWithMin.id = "chattingWithMin"+who;
    chattingWithMin.className = "chattingWithMin";

    var closeChatMin = document.createElement("img");
    closeChatMin.id = "closeChatMin" + who;
    closeChatMin.className = "closeChatMin chatWindowButton";
    closeChatMin.src = "images/cerrar.png";
    closeChatMin.onclick = function () {closeChat(who);};
    
    var maximizeChat = document.createElement("img");
    maximizeChat.id = "maximizeChat" + who;
    maximizeChat.className = "maximizeChat chatWindowButton";
    maximizeChat.src = "images/maximizar.png";
    maximizeChat.onclick = function () {toggleChat(who);};

    chatWindowMinBar.appendChild(chattingWithMin);
    chatWindowMinBar.appendChild(closeChatMin);
    chatWindowMinBar.appendChild(maximizeChat);

    chatWindowMin.appendChild(chatWindowMinBar);

    chatWindowTopLevel.appendChild(chatWindow);
    chatWindowTopLevel.appendChild(chatWindowMin);

    chatWindows.appendChild(chatWindowTopLevel);
    chatText.focus();

    $(".chatWindow").draggable({ handle : ".chatWindowTopBar"});    
    $(".chatWindowMin").draggable({ handle : ".chatWindowTopBarMin"});

    //si estoy en una reunion, escondo el icono de llamada.

}

/**
* "Cambia" una ventana de maximizada a minimizada y viceversa. Tenemos creados los dos elementos, y muestra uno o el otro según corresponda. Es decir, si esta visible el elemento ventana maximizada,
* lo oculta y muestra el elemento ventana minimizada y viceversa.
* 
* @method toggleChat
* @param who {string} Id de la ventana que queremos maximizar o minimizar.
*/
function toggleChat(who) {
    if(document.getElementById("chatWindow"+who).style.display == "block"){
        oculta("chatWindow"+who);
        muestra("chatWindowMin"+who);
        document.getElementById("chattingWithMin"+who).innerHTML = document.getElementById("chattingWith"+who).innerHTML;
        document.getElementById("chatWindowMin"+who).style.left = document.getElementById("chatWindow"+who).style.left;
        document.getElementById("chatWindowMin"+who).style.bottom = document.getElementById("chatWindow"+who).style.bottom;
    } else {
        muestra("chatWindow"+who);
        oculta("chatWindowMin"+who);
        document.getElementById("chattingWith"+who).innerHTML = document.getElementById("chattingWithMin"+who).innerHTML;
        document.getElementById("chatWindow"+who).style.left = document.getElementById("chatWindowMin"+who).style.left;
        document.getElementById("chatWindow"+who).style.bottom = document.getElementById("chatWindowMin"+who).style.bottom;
    }
}

/**
* @method closeChat
* @param who {string} Id de la ventana que queremos cerrar.
*/
function closeChat(who) {
    //oculta("chatWindowTopLevel"+who);
    oculta("chatWindow"+who);
    oculta("chatWindowMin"+who)
}

/**
* Funcion seteada como callback para cuando nos llega un chat. La seteamos en connect(): easyrtc.setPeerListener(addToConversation); <br>
* Como solo se llama sola, cuando nos llega un mensaje, la vamos a llamar nosotros cuando enviamos un mensaje y poner el mensaje enviado en la ventana correspondiente. Y así ver lo que nosotro escribimos, ya que si no la llamamos, solo veríamos los mensajes de los otros.
* Para poder distinguir en que ventana poner el texto, utilizamos el parametro who. <br>
* <!-- El parametro who va a ser : Si msgType="1", who=toWhom|roomName. Si msgType = "2", who=toWhom. Si msgType="3", who=roomName (que va a ser igual a connectedToRoom) -->
*
* @method addToConversation
* @param who {string} Id de la ventana a la que queremos agregar el texto enviado.
* @param msgType {string} Puede tomar valores "1", "2", "3", "Warning". según si es un texto que enviamos nosotros, nos enviaron p2p, nos enviaron multiparty, o un error respectivamente.
* @param content Texto que enviamos o recibimos.
*/
function addToConversation(who, msgType, content) {
    
    var quienB = document.createElement("b");
    var quien;
    // Escape html special characters, then add linefeeds.
    content = content.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;");
    content = content.replace(/\n/g, "<br />");
    var que = document.createTextNode(content);
    var br = document.createElement("br");
    var objDiv;
    var scrollMessage = false;

    switch(msgType){
        //Me envie a mi mismo un mensaje.
        case "1":
            //Ventana donde mande el mensaje (identificada con el id o el nombre de la sala)
            makeChatWindow(who);
            objDiv = document.getElementById("chatWindowText"+who);//who es id|roomName
            quien = document.createTextNode("Yo: ");
            break;

        //Me llego un mensaje p2p
        case "2":
            //Ventana de chat con el id de quien me mando el mensaje.
            makeChatWindow(who);
            objDiv = document.getElementById("chatWindowText"+who);
            quien = document.createTextNode(easyrtc.idToName(who)+": ");
            scrollMessage = true;
            break;

        //Me llego un msj de sala   
        case "3":
            //Ventana de chat con el nombre de la sala.
            makeChatWindow(connectedToRoom);
            objDiv = document.getElementById("chatWindowText"+connectedToRoom);
            quien = document.createTextNode(easyrtc.idToName(who)+": ");
            scrollMessage = true;
            break;

        //Error 
        case "Warning":
            //Ventana donde mande el mensaje (identificada con el id o el nombre de la sala)
            makeChatWindow(who);
            objDiv = document.getElementById("chatWindowText"+who);
            quien = document.createTextNode("Admin: ");
            break;
    }//switch

    quienB.appendChild(quien);
    objDiv.appendChild(quienB);
    objDiv.appendChild(que);
    objDiv.appendChild(br);
    objDiv.scrollTop = objDiv.scrollHeight;

    if(scrollMessage)
        addToScroller(easyrtc.idToName(who)+": "+content);
    
    document.getElementById('chatSound').play();
    StartBlinking(content);
}//function

/**
* Envia el texto ingresado a quien corresponda, y además llama a addToConversation para enviarnos a nosotros mismos y ver reflejado en nuestras ventanas lo que escribimos.
*
* @method sendChatText
* @param toWhom {string} Toma uno de los dos valores: easyrtcid ó nombre de la sala,  si estamos chateando p2p, o si es un chat multiparty.
*/
function sendChatText(toWhom) {
    var sent=0;
    var text = document.getElementById("chatText"+toWhom).value;

    if (text && text != ""){
        document.getElementById("chatText"+toWhom).value = "";

        if (toWhom === connectedToRoom)
        {
                addToConversation(connectedToRoom,"1",text); //Me envio a mi mismo el texto. Y como es un chat de sala, me envio el nombre de la sala donde irá el mensaje.
                for(var i in roomUsers) //Le envio el texto a todos los integrantes de la sala, tipo multiparty (msgType = "3").
                    easyrtc.sendPeerMessage(i,"3",text);
        } 
        else
        {
            addToConversation(toWhom,"Warning","Conexion no disponible.");
        }
    }//if
    
    document.getElementById("chatText"+toWhom).focus();
}//function


///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////******Video-windows handlers ***///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////
///////////////////////////********************************///////////////////////////



var activeBox = -1;  // nothing selected
var aspectRatio = 4/3;  // standard definition video aspect ratio
var maxCALLERS = 3;
var numVideoOBJS = maxCALLERS+1;
var layout;


easyrtc.dontAddCloseButtons(true);

function getIdOfBox(boxNum) {
    return "box" + boxNum;
}


function reshapeFull(parentw, parenth) {
    /*return {
        left:0,
        top:0,
        width:parentw,
        height:parenth
    };*/
    if (easyrtc.getConnectionCount() == 0) {
        return {
        left:0,
        top:0,
        width:150,
        height:150
        };
    } else{
        return {
        left:0,
        top:0,
        width:parentw,
        height:parenth
        };
    };
}



var margin = 20;

function reshapeToFullSize(parentw, parenth) {
    var left, top, width, height;
    var margin= 20;

    if( parentw < parenth*aspectRatio){
        width = parentw -margin;
        height = width/aspectRatio;
    }
    else {
        height = parenth-margin;
        width = height*aspectRatio;
    }
    left = (parentw - width)/2;
    top = (parenth - height)/2;
    /*return {
        left:left,
        top:top,
        width:width,
        height:height
    };*/
    if (easyrtc.getConnectionCount() == 0) {
        return {
        left:left,
        top:top,
        width:150,
        height:150
        };
    } else{
        return {
        left:left,
        top:top,
        width:width,
        height:height
        };
    };
}

//
// a negative percentLeft is interpreted as setting the right edge of the object
// that distance from the right edge of the parent.
// Similar for percentTop.
//
function setThumbSizeAspect(percentSize, percentLeft, percentTop, parentw, parenth, aspect) {

    var width, height;
    if( parentw < parenth*aspectRatio){
        width = parentw * percentSize;
        height = width/aspect;
    }
    else {
        height = parenth * percentSize;
        width = height*aspect;
    }
    var left;
    if( percentLeft < 0) {
        left = parentw - width;
    }
    else {
        left = 0;
    }
    left += Math.floor(percentLeft*parentw);
    var top = 0;
    if( percentTop < 0) {
        top = parenth - height;
    }
    else {
        top = 0;
    }
    top += Math.floor(percentTop*parenth);
    return {
        left:left,
        top:top,
        width:width,
        height:height
    };
}


function setThumbSize(percentSize, percentLeft, percentTop, parentw, parenth) {
    return setThumbSizeAspect(percentSize, percentLeft, percentTop, parentw, parenth, aspectRatio);
}

function setThumbSizeButton(percentSize, percentLeft, percentTop, parentw, parenth, imagew, imageh) {
    return setThumbSizeAspect(percentSize, percentLeft, percentTop, parentw, parenth, imagew/imageh);
}


var sharedVideoWidth  = 1;
var sharedVideoHeight = 1;

function reshape1of2(parentw, parenth) {
    if( layout== 'p' ) {
        return {
            left: (parentw-sharedVideoWidth)/2,
            top:  (parenth -sharedVideoHeight*2)/3,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        };
    }
    else {
        return{
            left: (parentw-sharedVideoWidth*2)/3,
            top:  (parenth -sharedVideoHeight)/2,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        }
    }
}



function reshape2of2(parentw, parenth){
    if( layout== 'p' ) {
        return {
            left: (parentw-sharedVideoWidth)/2,
            top:  (parenth -sharedVideoHeight*2)/3 *2 + sharedVideoHeight,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        };
    }
    else {
        return{
            left: (parentw-sharedVideoWidth*2)/3 *2 + sharedVideoWidth,
            top:  (parenth -sharedVideoHeight)/2,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        }
    }
}

function reshape1of3(parentw, parenth) {
    if( layout== 'p' ) {
        return {
            left: (parentw-sharedVideoWidth)/2,
            top:  (parenth -sharedVideoHeight*3)/4 ,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        };
    }
    else {
        return{
            left: (parentw-sharedVideoWidth*2)/3,
            top:  (parenth -sharedVideoHeight*2)/3,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        }
    }
}

function reshape2of3(parentw, parenth){
    if( layout== 'p' ) {
        return {
            left: (parentw-sharedVideoWidth)/2,
            top:  (parenth -sharedVideoHeight*3)/4*2+ sharedVideoHeight,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        };
    }
    else {
        return{
            left: (parentw-sharedVideoWidth*2)/3*2+sharedVideoWidth,
            top:  (parenth -sharedVideoHeight*2)/3,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        }
    }
}

function reshape3of3(parentw, parenth) {
    if( layout== 'p' ) {
        return {
            left: (parentw-sharedVideoWidth)/2,
            top:  (parenth -sharedVideoHeight*3)/4*3+ sharedVideoHeight*2,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        };
    }
    else {
        return{
            left: (parentw-sharedVideoWidth*2)/3*1.5+sharedVideoWidth/2,
            top:  (parenth -sharedVideoHeight*2)/3*2+ sharedVideoHeight,
            width: sharedVideoWidth,
            height: sharedVideoHeight
        }
    }
}


function reshape1of4(parentw, parenth) {
    return {
        left: (parentw - sharedVideoWidth*2)/3,
        top: (parenth - sharedVideoHeight*2)/3,
        width: sharedVideoWidth,
        height: sharedVideoHeight
    }
}

function reshape2of4(parentw, parenth) {
    return {
        left: (parentw - sharedVideoWidth*2)/3*2+ sharedVideoWidth,
        top: (parenth - sharedVideoHeight*2)/3,
        width: sharedVideoWidth,
        height: sharedVideoHeight
    }
}
function reshape3of4(parentw, parenth) {
    return {
        left: (parentw - sharedVideoWidth*2)/3,
        top: (parenth - sharedVideoHeight*2)/3*2 + sharedVideoHeight,
        width: sharedVideoWidth,
        height: sharedVideoHeight
    }
}

function reshape4of4(parentw, parenth) {
    return {
        left: (parentw - sharedVideoWidth*2)/3*2 + sharedVideoWidth,
        top: (parenth - sharedVideoHeight*2)/3*2 + sharedVideoHeight,
        width: sharedVideoWidth,
        height: sharedVideoHeight
    }
}

var boxUsed = [true, false, false, false];
var connectCount = 0;


function setSharedVideoSize(parentw, parenth) {
    layout = ((parentw /aspectRatio) < parenth)?'p':'l';
    var w, h;

    function sizeBy(fullsize, numVideos) {
        return (fullsize - margin*(numVideos+1) )/numVideos;
    }

    switch(layout+(connectCount+1)) {
        case 'p1':
        case 'l1':
            w = sizeBy(parentw, 1);
            h = sizeBy(parenth, 1);
            break;
        case 'l2':
            w = sizeBy(parentw, 2);
            h = sizeBy(parenth, 1);
            break;
        case 'p2':
            w = sizeBy(parentw, 1);
            h = sizeBy(parenth, 2);
            break;
        case 'p4':
        case 'l4':
        case 'l3':
            w = sizeBy(parentw, 2);
            h = sizeBy(parenth, 2);
            break;
        case 'p3':
            w = sizeBy(parentw, 1);
            h = sizeBy(parenth, 3);
            break;
    }
    sharedVideoWidth = Math.min(w, h * aspectRatio);
    sharedVideoHeight = Math.min(h, w/aspectRatio);
}

var reshapeThumbs = [
    function(parentw, parenth) {

        if( activeBox > 0 ) {
            return setThumbSize(0.20, 0.01, 0.01, parentw, parenth);
        }
        else {
            setSharedVideoSize(parentw, parenth)
            switch(connectCount) {
                case 0:return reshapeToFullSize(parentw, parenth);
                case 1:return reshape1of2(parentw, parenth);
                case 2:return reshape1of3(parentw, parenth);
                case 3:return reshape1of4(parentw, parenth);
            }
        }
    },
    function(parentw, parenth) {
        if( activeBox >= 0 || !boxUsed[1]) {
            return setThumbSize(0.20, 0.01, -0.01, parentw, parenth);
        }
        else{
            switch(connectCount) {
                case 1:
                    return reshape2of2(parentw, parenth);
                case 2:
                    return reshape2of3(parentw, parenth);
                case 3:
                    return reshape2of4(parentw, parenth);
            }
        }
    },
    function(parentw, parenth) {
        if( activeBox >= 0 || !boxUsed[2] ) {
            return setThumbSize(0.20, -0.01, 0.01, parentw, parenth);
        }
        else  {
            switch(connectCount){
                case 1:
                    return reshape2of2(parentw, parenth);
                case 2:
                    if( !boxUsed[1]) {
                        return reshape2of3(parentw, parenth);
                    }
                    else {
                        return reshape3of3(parentw, parenth);
                    }
                case 3:
                    return reshape3of4(parentw, parenth);
            }
        }
    },
    function(parentw, parenth) {
        if( activeBox >= 0 || !boxUsed[3]) {
            return setThumbSize(0.20, -0.01, -0.01, parentw, parenth);
        }
        else{
            switch(connectCount){
                case 1:
                    return reshape2of2(parentw, parenth);
                case 2:
                    return reshape3of3(parentw, parenth);
                case 3:
                    return reshape4of4(parentw, parenth);
            }
        }
    },
];


function setReshaper(elementId, reshapeFn) {
    var element = document.getElementById(elementId);
    if( !element) {
        alert("Attempt to apply to reshapeFn to non-existent element " + elementId);
    }
    if( !reshapeFn) {
        alert("Attempt to apply misnamed reshapeFn to element " + elementId);
    }
    element.reshapeMe = reshapeFn;
}


function collapseToThumbHelper() {
    if( activeBox >= 0) {
        var id = getIdOfBox(activeBox);
        document.getElementById(id).style.zIndex = 2;
        setReshaper(id, reshapeThumbs[activeBox]);
        activeBox = -1;
    }
}

function collapseToThumb() {
    collapseToThumbHelper();
    activeBox = -1;
    handleWindowResize();

}


function expandThumb(whichBox) {
    var lastActiveBox = activeBox;
    if( activeBox >= 0 ) {
        collapseToThumbHelper();
    }
    if( lastActiveBox != whichBox) {
        var id = getIdOfBox(whichBox);
        activeBox = whichBox;
        setReshaper(id, reshapeToFullSize);
        document.getElementById(id).style.zIndex = 1;
    }
    handleWindowResize();
}

function prepVideoBox(whichBox) {
    var id = getIdOfBox(whichBox);
    setReshaper(id, reshapeThumbs[whichBox]);
    document.getElementById(id).onclick = function() {
        expandThumb(whichBox);
    };
}

function killActiveBox() {
    if( activeBox > 0) {
        var easyrtcid = easyrtc.getIthCaller(activeBox-1);
        collapseToThumb();
        setTimeout( function() {
            easyrtc.hangup(easyrtcid);
        }, 400);
    }
}

function handleWindowResize() {
    document.getElementById('containerVideos').style.width = $("#main").width()-240 + "px";
    //document.getElementById('videos').style.width = $("#main").width()-240 + "px";
    var fullpage = document.getElementById('videos');
    fullpage.style.height = $("#containerVideos").height() + "px";
    fullpage.style.width = $("#containerVideos").width() + "px";
    connectCount = easyrtc.getConnectionCount();

    function applyReshape(obj,  parentw, parenth) {
        var myReshape = obj.reshapeMe(parentw, parenth);

        if(typeof myReshape.left !== 'undefined' ) {
            obj.style.left = Math.round(myReshape.left) + "px";
        }
        if(typeof myReshape.top !== 'undefined' ) {
            obj.style.top = Math.round(myReshape.top) + "px";
        }
        if(typeof myReshape.width !== 'undefined' ) {
            obj.style.width = Math.round(myReshape.width) + "px";
        }
        if(typeof myReshape.height !== 'undefined' ) {
            obj.style.height = Math.round(myReshape.height) + "px";
        }

        var n = obj.childNodes.length;
        for(var i = 0; i < n; i++ ) {
            var childNode = obj.childNodes[i];
            if( childNode.reshapeMe) {
                applyReshape(childNode, myReshape.width, myReshape.height);
            }
        }
    }

    applyReshape(fullpage, $("#containerVideos").width(), $("#containerVideos").height());
}