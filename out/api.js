YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "Base de datos.",
        "Conexion a la aplicacion",
        "Funciones auxiliares Sala",
        "Funciones auxiliares.",
        "Funciones de Llamada y Desconexion con Usuarios",
        "Funciones de Llamada y Desconexion con Usuarios Sala",
        "Funciones de Sala",
        "Funciones de callback  para cuando hay llamadas entrantes",
        "Funciones de callback  para cuando hay llamadas entrantes Sala",
        "Funciones de chat",
        "Funciones de chat Sala",
        "Funciones de conexion y desconexion al servidor",
        "Funciones de conexion y desconexion al servidor Sala",
        "Funciones de configuración de estado y texto personalizado de usuario",
        "Funciones de configuración de estado y texto personalizado de usuario Sala",
        "Funciones de file transefering",
        "Funciones de seteo y recuperación de valor de cookies.",
        "Override EasyRTC Listeners.",
        "Variables Globales",
        "Variables Globales Sala"
    ],
    "modules": [
        "Conferencias.js",
        "ConferenciasSala.js",
        "cookies.js",
        "server.js"
    ],
    "allModules": [
        {
            "displayName": "Conferencias.js",
            "name": "Conferencias.js",
            "description": "Implementacion de la logica de la aplicacion hecha por nosotros. <br>\nUtilizamos Modulo como un archivo. <br> Y a lo que le llama clases, \nsolamente son funciones que las agrupamos por la funcionalidad, pero no pertenecen a una clase per se."
        },
        {
            "displayName": "ConferenciasSala.js",
            "name": "ConferenciasSala.js",
            "description": "Implementacion de la logica de la aplicacion hecha por nosotros. <br>\nUtilizamos Modulo como un archivo. <br> Y a lo que le llama clases, \nsolamente son funciones que las agrupamos por la funcionalidad, pero no pertenecen a una clase per se."
        },
        {
            "displayName": "cookies.js",
            "name": "cookies.js",
            "description": "Implementacion de funciones de cookies."
        },
        {
            "displayName": "server.js",
            "name": "server.js",
            "description": "Servidor con conexión a base de datos MySQL"
        }
    ]
} };
});